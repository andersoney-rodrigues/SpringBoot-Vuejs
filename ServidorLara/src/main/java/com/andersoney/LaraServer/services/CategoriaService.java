/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.services;

import com.andersoney.LaraServer.model.Categoria;
import com.andersoney.LaraServer.model.ids.CategoriaId;
import com.andersoney.LaraServer.model.jobs.CategoriasListas;
import com.andersoney.LaraServer.model.jobs.Operacoes;
import com.andersoney.LaraServer.model.jobs.Tipo;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.andersoney.LaraServer.repository.CategoriaRepository;
import com.andersoney.LaraServer.repository.jobs.JobsCategoriesRepository;
import com.google.gson.Gson;
import java.util.Date;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ander
 */
@Service
public class CategoriaService {
    
    @Autowired
    CategoriaRepository categoriaNRepository;
    
    @Autowired
    JobsCategoriesRepository jobsCategoriesRepository;
    
    private static final Logger log = LoggerFactory.getLogger(CategoriaService.class);
    
    @Autowired
    UserService userService;
    
    @Transactional
    public Categoria cadastrar(Categoria object) {
        log.info("Acesso ao cadastro de categorias.");
        CategoriaId key = new CategoriaId(object.getCodecategory(), userService.getUserId());;
        object.setUserid(userService.getUserId());
        if (this.categoriaNRepository.findOne(key) == null) {
            object = this.categoriaNRepository.save(object);
            
            CategoriasListas categoriasListas = new CategoriasListas();
            Gson gson = new Gson();
            Date dataAtual = new Date();
            
            categoriasListas.setCategoria(object);
            categoriasListas.setCadastro(dataAtual);
            categoriasListas.setJson("" + gson.toJson(object) + "");
            categoriasListas.setOperacoes(Operacoes.CADASTRADO);
            categoriasListas.setTipo(Tipo.CATEGORIA);
            
            jobsCategoriesRepository.save(categoriasListas);
            
            return object;
        }
        throw new UnsupportedOperationException("Operação não suportada"); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Transactional
    public Page<Categoria> obterAllPageable(Pageable pageable) {
        Long userId = UserService.getUserId();
        log.info("Tentando pegar todas as categorias para usuario: " + userId);
        return categoriaNRepository.findAllByUserid(userId, pageable);
    }
    
    public Categoria obter(CategoriaId key) {
        return this.categoriaNRepository.findOne(key);
    }
    
    @Transactional
    public Categoria atualizar(CategoriaId key, Categoria object) {
        object.setUserid(userService.getUserId());
        if (this.categoriaNRepository.findOne(key) != null) {
            Categoria categoria = categoriaNRepository.findOne(key);
            if (categoria.equals(object)) {
                return object;
            } else {
                CategoriasListas categoriasListas = new CategoriasListas();
                Gson gson = new Gson();
                Date dataAtual = new Date();
                
                categoriasListas.setCategoria(object);
                categoriasListas.setCadastro(dataAtual);
                categoriasListas.setJson(gson.toJson(object));
                categoriasListas.setOperacoes(Operacoes.ATUALIZADO);
                categoriasListas.setTipo(Tipo.CATEGORIA);
                
                jobsCategoriesRepository.save(categoriasListas);
                
                object.setUserid(categoria.getUserid());
                Categoria save = categoriaNRepository.save(object);
                return save;
            }
        } else {
            throw new UnsupportedOperationException("Operação não suportada");
        }
    }
    
    @Transactional
    public Categoria excluir(CategoriaId key) {
        Categoria findOne = categoriaNRepository.findOne(key);
        this.categoriaNRepository.delete(key);
        return findOne;
    }
    
    @Transactional
    public List<Categoria> obterTodos() {
        return categoriaNRepository.findAll();
    }
    
}
