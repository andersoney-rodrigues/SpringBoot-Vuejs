/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.controller;

import com.andersoney.LaraServer.config.JWTFilter;
import com.andersoney.LaraServer.model.System.Cliente;
import com.andersoney.LaraServer.model.System.Usuario;
import com.andersoney.LaraServer.repository.ClienteRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.andersoney.LaraServer.repository.UsuarioRepository;

/**
 *
 * @author ander
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class HomeRestController {

    private final Logger log = LoggerFactory.getLogger(HomeRestController.class);

    @Autowired
    private UsuarioRepository appUserRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> login(@RequestBody Usuario user,
            HttpServletResponse response) throws IOException {
        String username = user.getUsername();
        String password = user.getPassword();
        String token = null;
        Usuario appUser = appUserRepository.findOneByUsername(username);
        Map<String, Object> tokenMap = new HashMap<String, Object>();
        if (appUser != null && passwordEncoder.matches(password, appUser.getPassword())) {
            Long expirationtime = System.currentTimeMillis() + JWTFilter.EXPIRATIONTIME;
            token = Jwts
                    .builder()
                    .setSubject(username)
                    .claim("roles", appUser.getRoles())
                    .setIssuedAt(new Date())
                    .signWith(SignatureAlgorithm.HS256, JWTFilter.SECRET_KEY)
                    .setExpiration(new Date(expirationtime))
                    .compact();
            tokenMap.put("expire_time", expirationtime);
            tokenMap.put("token", token);
            tokenMap.put("user", appUser);

            Cliente cliente = clienteRepository.findOne(appUser.getId());
            if (cliente != null) {
                List<String> faltas = new ArrayList<String>();
                if (cliente.getBancos().size() == 0) {
                    faltas.add("Falta preencher os dados Bancarios.");
                }
                if (cliente.getEmpresa() == null) {
                    faltas.add("Falta preencher os dados da Empresa.");
                }
                tokenMap.put("Falta", faltas);
            }
            log.info("Login com o Usuario: " + appUser.getUsername());
            return new ResponseEntity<Map<String, Object>>(tokenMap, HttpStatus.OK);
        } else {
            tokenMap.put("token", null);
            return new ResponseEntity<Map<String, Object>>(tokenMap, HttpStatus.UNAUTHORIZED);
        }

    }
}
