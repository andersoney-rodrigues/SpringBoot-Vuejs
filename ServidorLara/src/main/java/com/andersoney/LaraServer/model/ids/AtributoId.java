/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.ids;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author ander
 */
public class AtributoId implements Serializable {

    String codatributo;
    Long userid;

    public AtributoId() {
    }

    public AtributoId(String codatributo, Long userid) {
        this.codatributo = codatributo;
        this.userid = userid;
    }

    public String getCodatributo() {
        return codatributo;
    }

    public void setCodatributo(String codatributo) {
        this.codatributo = codatributo;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.codatributo);
        hash = 29 * hash + (int) (this.userid ^ (this.userid >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        final AtributoId other = (AtributoId) obj;
        if (this.userid != other.userid) {
            return false;
        }
        if (!Objects.equals(this.codatributo, other.codatributo)) {
            return false;
        }
        return true;
    }

}
