export let lista = [
    {
        code: "Marketplace-000000001",
        channel: "Marketplace",
        placed_at: "2016-06-10T09:46:04-03:00",
        updated_at: "2016-06-15T09:46:04-03:00",
        total_ordered: 107.68,
        interest: 2.69,
        shipping_cost: 15.0,
        shipping_method: "Correios - SEDEX",
        estimated_delivery: "2016-06-20T09:46:04-03:00",
        shipping_address: {
            street: "Rua Sacadura Cabral",
            number: "130",
            detail: "foo",
            neighborhood: "Centro",
            city: "Rio de Janeiro",
            region: "RJ",
            country: "BR",
            postcode: "20081262"
        },
        billing_address: {
            street: "Rua Sacadura Cabral",
            number: "130",
            detail: "Sala 404",
            neighborhood: "Centro",
            city: "Rio de Janeiro",
            region: "RJ",
            country: "BR",
            postcode: "20081262"
        },
        customer: {
            name: "Comprador Exemplo",
            email: "comprador@exemplo.com.br",
            date_of_birth: "1993-03-03",
            gender: "male",
            vat_number: "76860543817",
            phones: ["2137223902", "2137223902", "2137223902"],
            state_registration: "100000000001"
        },
        items: [
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            },
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            },
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            }
        ],
        status: {
            code: "shipped",
            label: "Pedido enviado",
            type: "SHIPPED"
        },
        invoices: [
            {
                key: "44444444444444444444444444444444444444444444",
                number: "444444444",
                line: "444",
                issue_date: "2016-06-13T16:43:07-03:00"
            }
        ],
        shipments: [
            {
                code: "ENVIO-54321",
                items: [
                    {
                        sku: "SEU SKU",
                        qty: 1
                    }
                ],
                tracks: [
                    {
                        code: "SS123456789BR",
                        carrier: "Correios",
                        method: "SEDEX"
                    }
                ]
            }
        ],
        sync_status: "SYNCED",
        calculation_type: "b2wentregacorreios"
    },
    {
        code: "Marketplace-000000001",
        channel: "Marketplace",
        placed_at: "2016-06-10T09:46:04-03:00",
        updated_at: "2016-06-15T09:46:04-03:00",
        total_ordered: 107.68,
        interest: 2.69,
        shipping_cost: 15.0,
        shipping_method: "Correios - SEDEX",
        estimated_delivery: "2016-06-20T09:46:04-03:00",
        shipping_address: {
            street: "Rua Sacadura Cabral",
            number: "130",
            detail: "foo",
            neighborhood: "Centro",
            city: "Rio de Janeiro",
            region: "RJ",
            country: "BR",
            postcode: "20081262"
        },
        billing_address: {
            street: "Rua Sacadura Cabral",
            number: "130",
            detail: "Sala 404",
            neighborhood: "Centro",
            city: "Rio de Janeiro",
            region: "RJ",
            country: "BR",
            postcode: "20081262"
        },
        customer: {
            name: "Comprador Exemplo",
            email: "comprador@exemplo.com.br",
            date_of_birth: "1993-03-03",
            gender: "male",
            vat_number: "76860543817",
            phones: ["2137223902", "2137223902", "2137223902"],
            state_registration: "100000000001"
        },
        items: [
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            },
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            },
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            }
        ],
        status: {
            code: "shipped",
            label: "Pedido enviado",
            type: "SHIPPED"
        },
        invoices: [
            {
                key: "44444444444444444444444444444444444444444444",
                number: "444444444",
                line: "444",
                issue_date: "2016-06-13T16:43:07-03:00"
            }
        ],
        shipments: [
            {
                code: "ENVIO-54321",
                items: [
                    {
                        sku: "SEU SKU",
                        qty: 1
                    }
                ],
                tracks: [
                    {
                        code: "SS123456789BR",
                        carrier: "Correios",
                        method: "SEDEX"
                    }
                ]
            }
        ],
        sync_status: "SYNCED",
        calculation_type: "b2wentregacorreios"
    },
    {
        code: "Marketplace-000000001",
        channel: "Marketplace",
        placed_at: "2016-06-10T09:46:04-03:00",
        updated_at: "2016-06-15T09:46:04-03:00",
        total_ordered: 107.68,
        interest: 2.69,
        shipping_cost: 15.0,
        shipping_method: "Correios - SEDEX",
        estimated_delivery: "2016-06-20T09:46:04-03:00",
        shipping_address: {
            street: "Rua Sacadura Cabral",
            number: "130",
            detail: "foo",
            neighborhood: "Centro",
            city: "Rio de Janeiro",
            region: "RJ",
            country: "BR",
            postcode: "20081262"
        },
        billing_address: {
            street: "Rua Sacadura Cabral",
            number: "130",
            detail: "Sala 404",
            neighborhood: "Centro",
            city: "Rio de Janeiro",
            region: "RJ",
            country: "BR",
            postcode: "20081262"
        },
        customer: {
            name: "Comprador Exemplo",
            email: "comprador@exemplo.com.br",
            date_of_birth: "1993-03-03",
            gender: "male",
            vat_number: "76860543817",
            phones: ["2137223902", "2137223902", "2137223902"],
            state_registration: "100000000001"
        },
        items: [
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            },
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            },
            {
                id: "sku001-01",
                product_id: "SEU SKU",
                name: "Produto exemplo",
                qty: 1,
                original_price: 99.99,
                special_price: 89.99
            }
        ],
        status: {
            code: "shipped",
            label: "Pedido enviado",
            type: "SHIPPED"
        },
        invoices: [
            {
                key: "44444444444444444444444444444444444444444444",
                number: "444444444",
                line: "444",
                issue_date: "2016-06-13T16:43:07-03:00"
            }
        ],
        shipments: [
            {
                code: "ENVIO-54321",
                items: [
                    {
                        sku: "SEU SKU",
                        qty: 1
                    }
                ],
                tracks: [
                    {
                        code: "SS123456789BR",
                        carrier: "Correios",
                        method: "SEDEX"
                    }
                ]
            }
        ],
        sync_status: "SYNCED",
        calculation_type: "b2wentregacorreios"
    }
]
export let novo = {
    code: "Marketplace-000000001",
    channel: "Marketplace",
    placed_at: "2016-06-10T09:46:04-03:00",
    updated_at: "2016-06-15T09:46:04-03:00",
    total_ordered: 107.68,
    interest: 2.69,
    shipping_cost: 15.0,
    shipping_method: "Correios - SEDEX",
    estimated_delivery: "2016-06-20T09:46:04-03:00",
    shipping_address: {
        street: "Rua Sacadura Cabral",
        number: "130",
        detail: "foo",
        neighborhood: "Centro",
        city: "Rio de Janeiro",
        region: "RJ",
        country: "BR",
        postcode: "20081262"
    },
    billing_address: {
        street: "Rua Sacadura Cabral",
        number: "130",
        detail: "Sala 404",
        neighborhood: "Centro",
        city: "Rio de Janeiro",
        region: "RJ",
        country: "BR",
        postcode: "20081262"
    },
    customer: {
        name: "Comprador Exemplo",
        email: "comprador@exemplo.com.br",
        date_of_birth: "1993-03-03",
        gender: "male",
        vat_number: "76860543817",
        phones: ["2137223902", "2137223902", "2137223902"],
        state_registration: "100000000001"
    },
    items: [
        {
            id: "sku001-01",
            product_id: "SEU SKU",
            name: "Produto exemplo",
            qty: 1,
            original_price: 99.99,
            special_price: 89.99
        },
        {
            id: "sku001-01",
            product_id: "SEU SKU",
            name: "Produto exemplo",
            qty: 1,
            original_price: 99.99,
            special_price: 89.99
        },
        {
            id: "sku001-01",
            product_id: "SEU SKU",
            name: "Produto exemplo",
            qty: 1,
            original_price: 99.99,
            special_price: 89.99
        }
    ],
    status: {
        code: "shipped",
        label: "Pedido enviado",
        type: "SHIPPED"
    },
    invoices: [
        {
            key: "44444444444444444444444444444444444444444444",
            number: "444444444",
            line: "444",
            issue_date: "2016-06-13T16:43:07-03:00"
        }
    ],
    shipments: [
        {
            code: "ENVIO-54321",
            items: [
                {
                    sku: "SEU SKU",
                    qty: 1
                }
            ],
            tracks: [
                {
                    code: "SS123456789BR",
                    carrier: "Correios",
                    method: "SEDEX"
                }
            ]
        }
    ],
    sync_status: "SYNCED",
    calculation_type: "b2wentregacorreios"
};