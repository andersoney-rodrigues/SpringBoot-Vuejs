/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.segundo;

import com.andersoney.LaraServer.SpringBootWebApplication;
import com.andersoney.LaraServer.TesteGeralJUnitTest;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author ander
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Teste2JUnitTest {

    private static final Logger log = LoggerFactory.getLogger(Teste2JUnitTest.class);

    public Teste2JUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void hello() {
        log.info("Inicio do teste");
        assertTrue(true);
    }
}
