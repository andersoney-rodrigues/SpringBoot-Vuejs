/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.Skyhub;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ander
 */
@Entity
public class SkyhubOrder implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;

    /**
     * Código de identificação do pedido. Esse código é gerado pela SkyHub e
     * consiste de duas partes separadas por um "-". A primeira parte contém o
     * nome do site que o pedido foi realizado (ex: "Submarino"), a segunda
     * parte contém o código do pedido gerado pelo marketplace. Caso você
     * precise, entre em contato com o suporte da SkyHub para ter acesso a lista
     * de nomes de todos os sites que podem aparecer na primeira parte do
     * código. IMPORTANTE: O nome do site que o pedido foi realizado pode conter
     * espaço. Exemplo: "Lojas Americanas-00001".
     */
    Long userid;

    String code;

    /**
     * Canal de vendas onde o pedido foi 'originado' (marketplace). Ex: 'Lojas
     * Americanas', 'Submarino', 'Extra', 'Walmart', 'Mercadolivre'
     */
    String channel;

    /**
     * Data e hora de criação do pedido
     */
    @Temporal(TemporalType.TIMESTAMP)
    Date placed_at;

    /**
     * Data e hora de atualização do pedido
     */
    @Temporal(TemporalType.TIMESTAMP)
    Date updated_at;

    /**
     * Valor total do pedido
     */
    Float total_ordered;

    /**
     * Valor de juros do pedido
     */
    Float interest;

    /**
     * Valor do frete
     */
    Float shipping_cost;

    /**
     * Método de envio do pedido. Alguns marketplaces só liberam o endereço de
     * entrega do pedido depois da confirmação de pagamento. Então para pedidos
     * aguardando confirmação de pagamento o campo método de envio pode não
     * aparecer.
     */
    String shipping_method;

    /**
     * Data estimada de entrega
     */
    @Temporal(TemporalType.TIMESTAMP)
    Date estimated_delivery;

    /**
     * Endereço de entrega. Alguns marketplaces só liberam o endereço de entrega
     * do pedido depois da confirmação de pagamento. Então para pedidos
     * aguardando confirmação de pagamento o campo endereço de entrega pode não
     * aparecer.
     */
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    SkyhubAddress shipping_address;
    /**
     * Endereço de cobrança
     */
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    SkyhubAddress billing_address;
    /**
     * Dados do cliente
     */
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    SkyhubCustomer customer;
    /**
     * Itens do pedido
     */
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    List<SkyhubSku> items;
    /**
     * SkyhubStatus do pedido
     */
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    SkyhubStatus status;
    /**
     * Informações de faturas
     */
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    List<Invoice> invoices;

    /**
     * Informações de envios
     */
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    List<SkyhubShipments> shipments;
    /**
     * SkyhubStatus de sincronização do pedido. Valores possíveis: "SYNCED",
     * "NOT_SYNCED" e "ERROR"
     */
    String sync_status;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Date getPlaced_at() {
        return placed_at;
    }

    public void setPlaced_at(Date placed_at) {
        this.placed_at = placed_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Float getTotal_ordered() {
        return total_ordered;
    }

    public void setTotal_ordered(Float total_ordered) {
        this.total_ordered = total_ordered;
    }

    public Float getInterest() {
        return interest;
    }

    public void setInterest(Float interest) {
        this.interest = interest;
    }

    public Float getShipping_cost() {
        return shipping_cost;
    }

    public void setShipping_cost(Float shipping_cost) {
        this.shipping_cost = shipping_cost;
    }

    public String getShipping_method() {
        return shipping_method;
    }

    public void setShipping_method(String shipping_method) {
        this.shipping_method = shipping_method;
    }

    public Date getEstimated_delivery() {
        return estimated_delivery;
    }

    public void setEstimated_delivery(Date estimated_delivery) {
        this.estimated_delivery = estimated_delivery;
    }

    public SkyhubAddress getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(SkyhubAddress shipping_address) {
        this.shipping_address = shipping_address;
    }

    public SkyhubAddress getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(SkyhubAddress billing_address) {
        this.billing_address = billing_address;
    }

    public SkyhubCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(SkyhubCustomer customer) {
        this.customer = customer;
    }

    public List<SkyhubSku> getItems() {
        return items;
    }

    public void setItems(List<SkyhubSku> items) {
        this.items = items;
    }

    public SkyhubStatus getStatus() {
        return status;
    }

    public void setStatus(SkyhubStatus status) {
        this.status = status;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public List<SkyhubShipments> getShipments() {
        return shipments;
    }

    public void setShipments(List<SkyhubShipments> shipments) {
        this.shipments = shipments;
    }

    public String getSync_status() {
        return sync_status;
    }

    public void setSync_status(String sync_status) {
        this.sync_status = sync_status;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

}
