/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ander
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"userid", "produtcCode"}, name = "produtoCodeUserIdUnic"))
public class Produto implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;

    private Long userid;
    String produtcCode;
    String name;
    String description;
    String status;
    Float cost;
    Float weight;
    Float length;
    Float height;
    String brand;
    String ean;
    @Column(nullable = true)
    String nbm;
    @Column(nullable = true)
    String returnCode;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<ProdutoAtributo> produtoAtributo;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<Sku> skus;
    @ElementCollection
    List<String> imagens;

    public Produto() {
        produtoAtributo = new ArrayList<ProdutoAtributo>();
        skus = new ArrayList<Sku>();
        imagens = new ArrayList<String>();
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getProdutcCode() {
        return produtcCode;
    }

    public void setProdutcCode(String produtcCode) {
        this.produtcCode = produtcCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getNbm() {
        return nbm;
    }

    public void setNbm(String nbm) {
        this.nbm = nbm;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public List<ProdutoAtributo> getProdutoAtributo() {
        return produtoAtributo;
    }

    public void setProdutoAtributo(List<ProdutoAtributo> produtoAtributo) {
        this.produtoAtributo = produtoAtributo;
    }

    public List<Sku> getSkus() {
        return skus;
    }

    public void setSkus(List<Sku> skus) {
        this.skus = skus;
    }

    public List<String> getImagens() {
        return imagens;
    }

    public void setImagens(List<String> imagens) {
        this.imagens = imagens;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

}
