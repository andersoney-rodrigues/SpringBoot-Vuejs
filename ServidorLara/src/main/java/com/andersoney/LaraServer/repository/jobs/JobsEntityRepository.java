/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.repository.jobs;

import com.andersoney.LaraServer.model.jobs.ListJobs;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ander
 */
public interface JobsEntityRepository extends JpaRepository<ListJobs, Long> {
    
}
