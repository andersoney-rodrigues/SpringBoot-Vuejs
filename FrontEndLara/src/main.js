// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import jQuery from 'jquery';
import VueResource from 'vue-resource'
// import Vuex from 'vuex'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        data: undefined,
        auth: false
    },
    mutations: {
        "INIT"(state) {
            var data = JSON.parse(sessionStorage.getItem('user'));
            if (data != null) {
                state.data = data;
                state.auth = true;
            } else {
                state.data = undefined;
                state.auth = false;
            }
        },
        "SET_DATA"(state, data) {
            state.data = data;
            state.auth = true;
        }
    }
});
store.commit("INIT");
console.log(store.state);
window.jQuery = window.$ = jQuery;
require("bootstrap-sass");
// Vue.use(Vuex)

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.state.auth) {
            if (
                store.state.data.expire_time <
                new Date().getTime() + 5000
            ) {
                console.log("Tempo do Token Expirou.");
                sessionStorage.clear();
                store.commit("INIT");
                next({ name: 'login', params: { redirect: to.name } })
            } else {
                next()
            }
        } else {
            next({ name: 'login', params: { redirect: to.name } })
        }
    } else {
        next()
    }
})

var baseUrl = "https://desolate-journey-56692.herokuapp.com";
// var baseUrl = "http://localhost:8090";
$.ajaxSetup({
    global: true,
    headers: {
    },
    beforeSend: function (xhr, options) {
        options.url = baseUrl + options.url;
        if (store.state.auth) {
            if (
                store.state.data.expire_time <
                new Date().getTime() + 5000
            ) {
                console.log("Tempo do Token Expirou.");
                sessionStorage.clear();
                store.commit("INIT");
                router.push({ name: "login" });
            }
        }
        xhr.setRequestHeader('Authorization', "Bearer " + sessionStorage.getItem("token"));
    },
    contentType: "application/json; charset=utf-8",
});
document.titulo = document.title
Vue.use(VueResource)
new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App }
})
