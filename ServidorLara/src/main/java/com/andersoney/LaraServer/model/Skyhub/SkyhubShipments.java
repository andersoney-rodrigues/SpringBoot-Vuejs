/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.Skyhub;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * Informações de envios
 *
 * @author ander
 */
@Entity
public class SkyhubShipments implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;

    /**
     * Código de identificação do envio
     */
    String code;
    /**
     * Itens do envio
     */
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    List<SkyhubItem> items;
    /**
     * Informações de rastreamento
     */
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    List<SkyhubTrack> tracks;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<SkyhubItem> getItems() {
        return items;
    }

    public void setItems(List<SkyhubItem> items) {
        this.items = items;
    }

    public List<SkyhubTrack> getTracks() {
        return tracks;
    }

    public void setTracks(List<SkyhubTrack> tracks) {
        this.tracks = tracks;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

}
