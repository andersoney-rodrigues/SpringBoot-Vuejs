/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.ids;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author ander
 */
public class CategoriaId implements Serializable {

    private Long userid;
    
    private String codecategory;

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getCodecategory() {
        return codecategory;
    }

    public CategoriaId() {
    }

    public CategoriaId(String codecategory, Long userid) {
        this.userid = userid;
        this.codecategory = codecategory;
    }

    public void setCodecategory(String codecategory) {
        this.codecategory = codecategory;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.codecategory);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CategoriaId other = (CategoriaId) obj;
        if (!Objects.equals(this.codecategory, other.codecategory) && !Objects.equals(getUserid(), other.getUserid())) {
            return false;
        }
        return true;
    }

}
