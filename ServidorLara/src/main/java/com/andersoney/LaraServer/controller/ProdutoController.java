/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.controller;

import com.andersoney.LaraServer.model.Produto;
import com.andersoney.LaraServer.model.ids.ProdutoId;
import com.andersoney.LaraServer.services.ProdutoService;
import com.andersoney.LaraServer.services.UserService;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ander
 */
@RestController
@RequestMapping("/api/produtos")
@PreAuthorize("hasRole('CLIENTE')")
public class ProdutoController implements ControllerDefault<Produto, Long> {

    private static final Logger log = LoggerFactory.getLogger(ProdutoController.class);
    @Autowired
    ProdutoService produtoService;

    @Override
    public ResponseEntity Atualizar(@PathVariable(value = "id") Long id, @RequestBody @Valid Produto object, BindingResult bindingResult) {
        log.info("Acesso ao POST de Categoria");
        log.info("" + object);
        if (bindingResult.hasErrors()) {
            for (FieldError fieldError : bindingResult.getFieldErrors()) {
                log.info("" + fieldError.getField());
            }
            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body("Objeto incompleto");
        } else {
            object = produtoService.atualizar(id, object);
            return ResponseEntity.status(HttpStatus.OK).body(object);
        }
    }

    @Override
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, path = "/")
    @ResponseBody
    public ResponseEntity cadastrar(@RequestBody @Valid Produto object, BindingResult bindingResult) {
        log.info("Acesso ao POST de Categoria");
        log.info("" + object);
        Long userid = UserService.getUserId();
        object.setUserid(userid);
        object = produtoService.cadastrar(object);
        return ResponseEntity.status(HttpStatus.OK).body(object);
    }

    @Override
    public ResponseEntity excluir(@PathVariable(value = "id") Long id) {
        Produto excluir = produtoService.excluir(id);
        return ResponseEntity.status(HttpStatus.OK).body("" + excluir);
    }

    @Override
    public Produto get(@PathVariable(value = "id") Long id) {
        return produtoService.obter(id);
    }

    @Override
    public Page<Produto> getAllPaginator(Pageable pageable) {
        Long userid = UserService.getUserId();
        return produtoService.obterAllPageable(userid, pageable);
    }

    @Override
    public List<Produto> obterTodos() {
        return produtoService.obterTodos();
    }

}
