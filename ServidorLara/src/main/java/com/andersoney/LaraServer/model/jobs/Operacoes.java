/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.jobs;

/**
 *
 * @author ander
 */
public enum Operacoes {
    CADASTRADO(1),
    ATUALIZADO(2);

    private final Integer operacao;

    private Operacoes(Integer operacao) {
        this.operacao = operacao;
    }

}
