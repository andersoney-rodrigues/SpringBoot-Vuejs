/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.Skyhub;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Dados do cliente
 *
 * @author ander
 */
@Entity
public class SkyhubCustomer implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;

    /**
     * Nome
     */
    String name;
    /**
     * Email
     */
    String email;
    /**
     * Data de nascimento
     */
    Date date_of_birth;
    /**
     * Sexo (male ou female)
     */
    String gender;
    /**
     * CPF ou CNPJ
     */
    String vat_number;
    /**
     * Lista de telefones
     */
    @ElementCollection(fetch = FetchType.EAGER)
    List<String> phones;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getVat_number() {
        return vat_number;
    }

    public void setVat_number(String vat_number) {
        this.vat_number = vat_number;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

}
