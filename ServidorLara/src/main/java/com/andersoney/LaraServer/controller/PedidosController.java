/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.controller;

import com.andersoney.LaraServer.model.Skyhub.SkyhubOrder;
import com.andersoney.LaraServer.services.SkyhubOrdersService;
import com.andersoney.LaraServer.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ander
 */
@RestController
@RequestMapping("/api/pedidos")
@PreAuthorize("hasRole('CLIENTE')")
public class PedidosController {

    private static final Logger log = LoggerFactory.getLogger(ProdutoController.class);
    @Autowired
    SkyhubOrdersService skyhubOrdersService;

    @PreAuthorize("hasRole('CLIENTE')")
    @GetMapping
    @ResponseBody
    public Page<SkyhubOrder> getAllPaginator(Pageable pageable) {
        Long userid = UserService.getUserId();
        return skyhubOrdersService.obterAllPageable(userid, pageable);
    }
}
