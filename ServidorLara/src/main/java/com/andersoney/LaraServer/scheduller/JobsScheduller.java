/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.scheduller;

import com.andersoney.LaraServer.config.Urls.UrlsSkyhub;
import com.andersoney.LaraServer.model.Skyhub.SkyhubOrder;
import com.andersoney.LaraServer.model.System.Usuario;
import com.andersoney.LaraServer.model.jobs.ListJobs;
import com.andersoney.LaraServer.repository.UsuarioRepository;
import com.andersoney.LaraServer.repository.jobs.JobsCategoriesRepository;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.andersoney.LaraServer.repository.jobs.JobsEntityRepository;
import com.andersoney.LaraServer.repository.jobs.JobsProductRepository;
import com.andersoney.LaraServer.services.SkyhubOrdersService;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.util.ArrayList;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ander
 */
@Component
public class JobsScheduller {

    private static final Logger log = LoggerFactory.getLogger(JobsScheduller.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

//    @Autowired
//    JobsEntityRepository jobsRepository;
//
//    @Autowired
//    JobsCategoriesRepository jobsCategoriesRepository;
//
//    @Autowired
//    JobsProductRepository jobsProductRepository;
//
//    @Autowired
//    private UrlsSkyhub urlsSkyhub;
//    public void Jobs() {
//        log.trace("Start JobsScheduller in: {}", dateFormat.format(new Date()));
//        List<ListJobs> findAll = jobsRepository.findAll();
//        for (ListJobs listJobs : findAll) {
//            log.trace("" + listJobs.getJson());
//        }
//        log.trace("Informações\n" + urlsSkyhub.getProducts());
//        //        final String uri = "http://localhost:8080/springrestexample/employees.xml";
//        //
//        //        RestTemplate restTemplate = new RestTemplate();
//        //        String result = restTemplate.getForObject(uri, String.class);
//        //
//        //        System.out.println(result);
//    }
    @Autowired
    SkyhubOrdersService skyhubOrdersService;

    @Autowired
    private UsuarioRepository appUserRepository;

    @Scheduled(initialDelay = 30 * 1000L, fixedRate = 4 * 60 * 60 * 1000L)
    public void hello() {
        if (skyhubOrdersService.count() < 5) {
            Gson gson = new Gson();
            log.info("Inicio do teste");
            List<SkyhubOrder> list = new ArrayList<SkyhubOrder>();
            Usuario user = appUserRepository.findOneByUsername("administrador");
            for (int i = 0; i < 10; i++) {
                SkyhubOrder objeto = novoSkuhubOrders();
                skyhubOrdersService.cadastrar(objeto, user.getId());
            }
            log.info("Salvo todas as Orders");
        }
    }

    private SkyhubOrder novoSkuhubOrders() throws JsonSyntaxException {
        String json = "{\n"
                + "        code: \"Marketplace-000000001\",\n"
                + "        channel: \"Marketplace\",\n"
                + "        placed_at: \"2016-06-10T09:46:04-03:00\",\n"
                + "        updated_at: \"2016-06-15T09:46:04-03:00\",\n"
                + "        total_ordered: 107.68,\n"
                + "        interest: 2.69,\n"
                + "        shipping_cost: 15.0,\n"
                + "        shipping_method: \"Correios - SEDEX\",\n"
                + "        estimated_delivery: \"2016-06-20T09:46:04-03:00\",\n"
                + "        shipping_address: {\n"
                + "            street: \"Rua Sacadura Cabral\",\n"
                + "            number: \"130\",\n"
                + "            detail: \"foo\",\n"
                + "            neighborhood: \"Centro\",\n"
                + "            city: \"Rio de Janeiro\",\n"
                + "            region: \"RJ\",\n"
                + "            country: \"BR\",\n"
                + "            postcode: \"20081262\"\n"
                + "        },\n"
                + "        billing_address: {\n"
                + "            street: \"Rua Sacadura Cabral\",\n"
                + "            number: \"130\",\n"
                + "            detail: \"Sala 404\",\n"
                + "            neighborhood: \"Centro\",\n"
                + "            city: \"Rio de Janeiro\",\n"
                + "            region: \"RJ\",\n"
                + "            country: \"BR\",\n"
                + "            postcode: \"20081262\"\n"
                + "        },\n"
                + "        customer: {\n"
                + "            name: \"Comprador Exemplo\",\n"
                + "            email: \"comprador@exemplo.com.br\",\n"
                + "            date_of_birth: \"1993-03-03\",\n"
                + "            gender: \"male\",\n"
                + "            vat_number: \"76860543817\",\n"
                + "            phones: [\"2137223902\", \"2137223902\", \"2137223902\"],\n"
                + "            state_registration: \"100000000001\"\n"
                + "        },\n"
                + "        items: [\n"
                + "            {\n"
                + "                id: \"sku001-01\",\n"
                + "                product_id: \"SEU SKU\",\n"
                + "                name: \"Produto exemplo\",\n"
                + "                qty: 1,\n"
                + "                original_price: 99.99,\n"
                + "                special_price: 89.99\n"
                + "            },\n"
                + "            {\n"
                + "                id: \"sku001-01\",\n"
                + "                product_id: \"SEU SKU\",\n"
                + "                name: \"Produto exemplo\",\n"
                + "                qty: 1,\n"
                + "                original_price: 99.99,\n"
                + "                special_price: 89.99\n"
                + "            },\n"
                + "            {\n"
                + "                id: \"sku001-01\",\n"
                + "                product_id: \"SEU SKU\",\n"
                + "                name: \"Produto exemplo\",\n"
                + "                qty: 1,\n"
                + "                original_price: 99.99,\n"
                + "                special_price: 89.99\n"
                + "            }\n"
                + "        ],\n"
                + "        status: {\n"
                + "            code: \"shipped\",\n"
                + "            label: \"Pedido enviado\",\n"
                + "            type: \"SHIPPED\"\n"
                + "        },\n"
                + "        invoices: [\n"
                + "            {\n"
                + "                key: \"4444444\",\n"
                + "                number: \"4444444\",\n"
                + "                line: \"444\",\n"
                + "                issue_date: \"2016-06-13T16:43:07-03:00\"\n"
                + "            }\n"
                + "        ],\n"
                + "        shipments: [\n"
                + "            {\n"
                + "                code: \"ENVIO-54321\",\n"
                + "                items: [\n"
                + "                    {\n"
                + "                        sku: \"SEU SKU\",\n"
                + "                        qty: 1\n"
                + "                    }\n"
                + "                ],\n"
                + "                tracks: [\n"
                + "                    {\n"
                + "                        code: \"SS123456789BR\",\n"
                + "                        carrier: \"Correios\",\n"
                + "                        method: \"SEDEX\"\n"
                + "                    }\n"
                + "                ]\n"
                + "            }\n"
                + "        ],\n"
                + "        sync_status: \"SYNCED\",\n"
                + "        calculation_type: \"b2wentregacorreios\"\n"
                + "    }";
        Gson gson = new Gson();
        SkyhubOrder objeto = gson.fromJson(json, SkyhubOrder.class);
        return objeto;
    }
}
