/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.services;

import com.andersoney.LaraServer.model.Skyhub.SkyhubOrder;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.andersoney.LaraServer.repository.SkuhubOrdersRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ander
 */
@Service
public class SkyhubOrdersService {

    @Autowired
    SkuhubOrdersRepository skuhubOrdersRepository;

    @Transactional
    public long count() {
        return skuhubOrdersRepository.count();
    }

    @Transactional
    public SkyhubOrder cadastrar(SkyhubOrder object) {
        try {
            Long userId = UserService.getUserId();
            object.setUserid(userId);
        } catch (Exception e) {
            object.setUserid(UserService.getUserIdAdm());
        }
        return skuhubOrdersRepository.save(object);
    }

    @Transactional
    public Page<SkyhubOrder> obterAllPageable(Long userid, Pageable pageable) {
        return skuhubOrdersRepository.findByUserid(userid, pageable);
    }

    @Transactional
    public SkyhubOrder obter(Long id) {
        return skuhubOrdersRepository.findOne(id);
    }

    @Transactional
    public SkyhubOrder atualizar(Long id, SkyhubOrder produto) {
        Long userId = UserService.getUserId();
        SkyhubOrder one = skuhubOrdersRepository.findOne(id);
        if (one.getUserid() == userId) {
            produto.setID(one.getID());
            return skuhubOrdersRepository.save(produto);
        } else {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Transactional
    public SkyhubOrder excluir(Long id) {
        Long userId = UserService.getUserId();
        SkyhubOrder one = skuhubOrdersRepository.findOne(id);
        if (one.getUserid() == userId) {
            skuhubOrdersRepository.delete(id);
            return one;
        } else {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Transactional
    public List<SkyhubOrder> obterTodos() {
        Long userid;
        try {
            userid = UserService.getUserId();
        } catch (Exception e) {
            userid = UserService.getUserIdAdm();
        }
        List<SkyhubOrder> allOrders = this.skuhubOrdersRepository.findByUserid(userid);
        return allOrders;
    }

    @Transactional
    public boolean setListSkyhubOrder(List<SkyhubOrder> list, Long userId) {
        for (SkyhubOrder skyhubOrder : list) {
            skyhubOrder.setID(userId);
        }
        this.skuhubOrdersRepository.save(list);
        return true;
    }

    @Transactional
    public SkyhubOrder cadastrar(SkyhubOrder list, Long userid) {
        list.setUserid(userid);
        this.skuhubOrdersRepository.save(list);
        return list;
    }
}
