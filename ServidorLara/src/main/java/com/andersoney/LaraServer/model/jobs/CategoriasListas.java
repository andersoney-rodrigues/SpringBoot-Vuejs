/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.jobs;

import com.andersoney.LaraServer.model.Categoria;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 *
 * @author ander
 */
@Entity
public class CategoriasListas extends ListJobs implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    Categoria categoria;

    public CategoriasListas() {
//        super();
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}
