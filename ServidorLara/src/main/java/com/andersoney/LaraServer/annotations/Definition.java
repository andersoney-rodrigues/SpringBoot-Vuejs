/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.annotations;

/**
 *
 * @author andersoney.santos
 */
public @interface Definition {

    public enum Priority {

    }

    String createdBy() default "Andersoney";

    String lastModified() default "01/12/2017";
}
