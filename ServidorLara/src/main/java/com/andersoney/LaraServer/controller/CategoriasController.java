/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.controller;

import com.andersoney.LaraServer.model.Categoria;
import com.andersoney.LaraServer.model.ids.CategoriaId;
import com.andersoney.LaraServer.services.CategoriaService;
import com.andersoney.LaraServer.services.DefaultService;
import com.andersoney.LaraServer.services.UserService;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ander
 */
@RestController
@RequestMapping("/api/categorias")
@PreAuthorize("hasRole('CLIENTE')")
public class CategoriasController implements ControllerDefault<Categoria, String> {

    private static final Logger log = LoggerFactory.getLogger(DefaultService.class);
    @Autowired
    CategoriaService categoriaService;
    @Autowired
    UserService userService;

    @Override
    public ResponseEntity Atualizar(@PathVariable(value = "id") String id, @RequestBody
            @Valid Categoria object, BindingResult bindingResult) {
        log.info("Acesso ao PUT de Categoria id: " + id);
        if (bindingResult.hasErrors()) {
            for (FieldError fieldError : bindingResult.getFieldErrors()) {
                System.out.println("" + fieldError.getField());
            }
            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body("Falha no preenchimento dos campos");
        } else {
            CategoriaId key = new CategoriaId(id, userService.getUserId());
            object = categoriaService.atualizar(key, object);
            return ResponseEntity.status(HttpStatus.OK).body(object);
        }
    }

    @Override
    public ResponseEntity cadastrar(@RequestBody @Valid Categoria object, BindingResult bindingResult) {
        log.info("Acesso ao POST de Categoria");
        log.info("" + object);
        if (bindingResult.hasErrors()) {
            for (FieldError fieldError : bindingResult.getFieldErrors()) {
                log.info("" + fieldError.getField());
            }
            return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body("Objeto incompleto");
        } else {
            object = categoriaService.cadastrar(object);
            return ResponseEntity.status(HttpStatus.OK).body(object);
        }
    }

    @Override
    public ResponseEntity excluir(@PathVariable(value = "id") String id) {
        CategoriaId key = new CategoriaId(id, userService.getUserId());
        return ResponseEntity.status(HttpStatus.OK).body(categoriaService.excluir(key));
    }

    @Override
    public Categoria get(@PathVariable(value = "id") String id) {
        CategoriaId key = new CategoriaId(id, userService.getUserId());
        return categoriaService.obter(key);
    }

    @Override
    public Page<Categoria> getAllPaginator(Pageable pageable) {
        return categoriaService.obterAllPageable(pageable);
    }

    @Override
    public List<Categoria> obterTodos() {
        log.info("Acesso ao GET de categorias.");
        return categoriaService.obterTodos();
    }
}
