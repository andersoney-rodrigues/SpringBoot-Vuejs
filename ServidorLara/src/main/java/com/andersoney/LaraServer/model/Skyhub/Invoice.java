package com.andersoney.LaraServer.model.Skyhub;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Informações de faturas
 *
 * @author Adriano
 */
@Entity
public class Invoice implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "numero")
    private Double number;
    @Column(name = "linha")
    private String line;

    private Date issueDate;
    @Column(name = "chave")
    private String key;

    private String danfeXml;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDanfeXml() {
        return danfeXml;
    }

    public void setDanfeXml(String danfeXml) {
        this.danfeXml = danfeXml;
    }

}
