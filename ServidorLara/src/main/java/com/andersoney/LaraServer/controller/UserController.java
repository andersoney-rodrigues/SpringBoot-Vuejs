/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.controller;

import com.andersoney.LaraServer.model.System.Banco;
import com.andersoney.LaraServer.model.System.Cliente;
import com.andersoney.LaraServer.model.System.Empresa;
import com.andersoney.LaraServer.model.System.Usuario;
import com.andersoney.LaraServer.services.ClienteService;
import com.andersoney.LaraServer.services.UserService;
import java.io.IOException;
import java.security.Principal;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ander
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

    private final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    ClienteService clienteService;

    @PreAuthorize("hasRole('ADM')")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Cliente> createUser(@RequestBody Cliente user) {
        return new ResponseEntity<Cliente>(clienteService.cadastrar(user), HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('CLIENTE')")
    @RequestMapping("/user")
    public Usuario user(Principal principal) {
        return clienteService.getUsuario();
    }

    @PreAuthorize("hasRole('CLIENTE')")
    @RequestMapping("/cliente")
    public Cliente cliente() {
        Long userid = UserService.getUserId();
        return clienteService.getCliente();
    }

    @RequestMapping(value = "/cliente", method = RequestMethod.POST)
    public Cliente clientePost(@RequestBody Cliente cliente,
            HttpServletResponse response) throws IOException {
        return clienteService.atualizar(cliente);
    }

    @PreAuthorize("hasRole('CLIENTE')")
    @RequestMapping("/banco")
    public Banco banco() {
        return clienteService.getBanco();
    }

    @RequestMapping(value = "/banco", method = RequestMethod.POST)
    public Banco bancoPost(@RequestBody Banco banco,
            HttpServletResponse response) throws IOException {
        return clienteService.atualizarBanco(banco);
    }

    @PreAuthorize("hasRole('CLIENTE')")
    @RequestMapping("/empresa")
    public Empresa empresa() {
        return clienteService.getEmpresa();
    }

    @RequestMapping(value = "/empresa", method = RequestMethod.POST)
    public Empresa empresaPost(@RequestBody Empresa empresa,
            HttpServletResponse response) throws IOException {
        return clienteService.atualizarEmpresa(empresa);
    }
}
