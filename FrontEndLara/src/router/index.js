import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/rotas/home/Home";

// Produtos
import ListaProduto from "@/components/rotas/produtos/Listagem";
import NovoProduto from "@/components/rotas/produtos/Cadastrar";
import VisualizarProduto from "@/components/rotas/produtos/Visualizar";
import EditarProduto from "@/components/rotas/produtos/Editar";

// Categorias
import ListaCategorias from "@/components/rotas/categorias/Listagem";
import NovoCategorias from "@/components/rotas/categorias/Cadastrar";
import VisualizarCategorias from "@/components/rotas/categorias/Visualizar";
import EditarCategorias from "@/components/rotas/categorias/Editar";


// Autenticação
import Login from "@/components/rotas/autenticacao/Login";
import Register from "@/components/rotas/autenticacao/Register";

import ConfigUsuario from "@/components/rotas/config/DadosDoUsuario";
import ConfigEmpresa from "@/components/rotas/config/DadosDaEmpresa";
import ConfigBanco from "@/components/rotas/config/DadosBancarios";

import pedidosListagem from "@/components/rotas/pedidos/Listagem";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: "/",
            name: "Home",
            component: Home,
            meta: { requiresAuth: true }
        },
        {
            path: "/produtos",
            name: "listaProdutos",
            component: ListaProduto,
            meta: { requiresAuth: true }
        },
        {
            path: "/produtos/novo",
            name: "novoProduto",
            component: NovoProduto,
            meta: { requiresAuth: true }
        },
        {
            path: "/produtos/:id",
            name: "visualizarProduto",
            component: VisualizarProduto,
            meta: { requiresAuth: true }
        },
        {
            path: "/produtos/:id/editar",
            name: "editarProduto",
            component: EditarProduto,
            meta: { requiresAuth: true }
        },
        {
            path: "/categorias",
            name: "listaCategorias",
            component: ListaCategorias,
            meta: { requiresAuth: true }
        },
        {
            path: "/categorias/novo",
            name: "novoCategorias",
            component: NovoCategorias,
            meta: { requiresAuth: true }
        },
        {
            path: "/categorias/:id",
            name: "visualizarCategorias",
            component: VisualizarCategorias,
            meta: { requiresAuth: true }
        },
        {
            path: "/categorias/:id/editar",
            name: "editarCategorias",
            component: EditarCategorias,
            meta: { requiresAuth: true }
        },
        {
            path: "/login",
            name: "login",
            component: Login
        },
        {
            path: "/register",
            name: "register",
            component: Register,
            meta: { requiresAuth: true }
        },
        {
            path: "/config/usuario",
            name: "configUser",
            component: ConfigUsuario,
            meta: { requiresAuth: true }
        },
        {
            path: "/config/empresa",
            name: "configEmpresa",
            component: ConfigEmpresa,
            meta: { requiresAuth: true }
        },
        {
            path: "/config/banco",
            name: "configBanco",
            component: ConfigBanco,
            meta: { requiresAuth: true }
        },
        {
            path: "/pedidos",
            name: "pedidosListagem",
            component: pedidosListagem,
            meta: { requiresAuth: true }
        },
    ]
});

