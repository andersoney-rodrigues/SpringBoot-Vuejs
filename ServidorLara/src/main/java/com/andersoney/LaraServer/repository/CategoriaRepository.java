/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.repository;

import com.andersoney.LaraServer.model.Categoria;
import com.andersoney.LaraServer.model.ids.CategoriaId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ander
 */
public interface CategoriaRepository extends JpaRepository<Categoria, CategoriaId> {

    public Page<Categoria> findAllByUserid(Long userid, Pageable pageable);

}
