/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.scheduller;

import com.andersoney.LaraServer.model.Skyhub.SkyhubOrder;
import com.andersoney.LaraServer.model.System.Usuario;
import com.andersoney.LaraServer.repository.SkuhubOrdersRepository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.andersoney.LaraServer.repository.UsuarioRepository;
import com.google.gson.Gson;

/**
 *
 * @author andersoney.santos
 */
@Component
public class UserAdminConfig {

    private static final Logger log = LoggerFactory.getLogger(UserAdminConfig.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private UsuarioRepository appUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    SkuhubOrdersRepository skuhubOrdersRepository;

    @Scheduled(initialDelay = 1000L, fixedRate = 24 * 60 * 60 * 1000L)
    @Transactional
    public void InitialUser() {
        log.debug("Start UserAdminConfig in: {}", dateFormat.format(new Date()));

        testeLog();
        Usuario user = appUserRepository.findOneByUsername("administrador");
        log.debug("" + user);
        if (user != null) {
            log.info("Administrador já existente no sistema.");
        } else {
            Usuario adm = new Usuario();
            adm.setPassword("admin123");
            adm.setPassword(passwordEncoder.encode(adm.getPassword()));
            adm.setUsername("administrador");
            List<String> roles = new ArrayList<String>();
            roles.add("USER");
            roles.add("ADM");
            adm.setRoles(roles);
            log.debug("" + adm);
            appUserRepository.save(adm);
            appUserRepository.flush();
            log.info("Administrador cadastrado.");
        }
    }

    private void testeLog() {
        String value = dateFormat.format(new Date());
        log.trace("doStuff needed more information - {}", value);
        log.debug("doStuff needed to debug - {}", value);
        log.info("doStuff took input - {}", value);
        log.warn("doStuff needed to warn - {}", value);
        log.error("doStuff encountered an error with value - {}", value);
    }

}
