/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.System;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import org.hibernate.annotations.CollectionType;

/**
 *
 * @author ander
 */
@Entity
public class Empresa implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;

    @Column(unique = true)
    private String CNPJ;

    private String razaoSocial;

    private String fantasia;

    private String inscMunicipal;

    private String inscEstadual;

    private String site;

    private String faturamentoAnual;

    private String endereco;

    private String numero;

    private String bairro;

    private String cep;

    private String cidade;

    private String estado;

    private String contato;

    @Column(unique = true)
    private String cpf;

    private String email;

    private String telefoneFixo;

    private String celular;

    @OneToOne(cascade = CascadeType.ALL)
    Contato comercial;

    @OneToOne(cascade = CascadeType.ALL)
    Contato financeiro;

    @OneToOne(cascade = CascadeType.ALL)
    Contato operacoes;

    @OneToOne(cascade = CascadeType.ALL)
    Contato ti;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    Date cadastro;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    Date inicioAtivacao;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    Date inicioVendas;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    Date dataFim;

    public Empresa() {
//        comercial = new Contato();
//        financeiro = new Contato();
//        operacoes = new Contato();
//        ti = new Contato();
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public String getInscMunicipal() {
        return inscMunicipal;
    }

    public void setInscMunicipal(String inscMunicipal) {
        this.inscMunicipal = inscMunicipal;
    }

    public String getInscEstadual() {
        return inscEstadual;
    }

    public void setInscEstadual(String inscEstadual) {
        this.inscEstadual = inscEstadual;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getFaturamentoAnual() {
        return faturamentoAnual;
    }

    public void setFaturamentoAnual(String faturamentoAnual) {
        this.faturamentoAnual = faturamentoAnual;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefoneFixo() {
        return telefoneFixo;
    }

    public void setTelefoneFixo(String telefoneFixo) {
        this.telefoneFixo = telefoneFixo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Date getCadastro() {
        return cadastro;
    }

    public void setCadastro(Date cadastro) {
        this.cadastro = cadastro;
    }

    public Date getInicioAtivacao() {
        return inicioAtivacao;
    }

    public void setInicioAtivacao(Date inicioAtivacao) {
        this.inicioAtivacao = inicioAtivacao;
    }

    public Date getInicioVendas() {
        return inicioVendas;
    }

    public void setInicioVendas(Date inicioVendas) {
        this.inicioVendas = inicioVendas;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Contato getComercial() {
        if (comercial == null) {
            return new Contato();
        } else {
            return comercial;
        }
    }

    public void setComercial(Contato comercial) {
        this.comercial = comercial;
    }

    public Contato getFinanceiro() {
        if (financeiro == null) {
            return new Contato();
        } else {
            return financeiro;
        }
    }

    public void setFinanceiro(Contato financeiro) {
        this.financeiro = financeiro;
    }

    public Contato getOperacoes() {
        if (operacoes == null) {
            return new Contato();
        } else {
            return operacoes;
        }
    }

    public void setOperacoes(Contato operacoes) {
        this.operacoes = operacoes;
    }

    public Contato getTi() {
        if (ti == null) {
            return new Contato();
        } else {
            return ti;
        }
    }

    public void setTi(Contato ti) {
        this.ti = ti;
    }

}
