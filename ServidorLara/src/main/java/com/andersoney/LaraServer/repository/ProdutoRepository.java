/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.repository;

import com.andersoney.LaraServer.model.Produto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author ander
 */
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

    @Query(value = "select p.id , p.produtcCode , p.name , p.description from Produto p where p.userid = ?1",
            countQuery = "SELECT count(*) FROM Produto p WHERE p.userid = ?1")
    public Page<Produto> findAll(Long userid, Pageable pageable);
}
