/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.ids;

import com.andersoney.LaraServer.services.UserService;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author ander
 */
public class ProdutoId implements Serializable {

    String produtcCode;
    private Long userid;

    public ProdutoId() {
    }

    public ProdutoId(String produtcCode, Long userid) {
        this.produtcCode = produtcCode;
        this.userid = userid;
    }

    public String getProdutcCode() {
        return produtcCode;
    }

    public void setProdutcCode(String produtcCode) {
        this.produtcCode = produtcCode;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.produtcCode);
        hash = 71 * hash + Objects.hashCode(this.userid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProdutoId other = (ProdutoId) obj;
        if (!Objects.equals(this.produtcCode, other.produtcCode)) {
            return false;
        }
        if (!Objects.equals(this.userid, other.userid)) {
            return false;
        }
        return true;
    }
    

}
