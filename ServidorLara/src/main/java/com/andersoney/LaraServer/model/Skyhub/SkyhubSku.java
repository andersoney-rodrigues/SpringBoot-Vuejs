/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.Skyhub;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Itens do pedido
 *
 * @author ander
 */
@Entity(name = "skyhub_sku")
public class SkyhubSku implements Serializable {

    /**
     * Identificador do item. Se o produto possuir variação, será o sku da
     * variação. Se o item não possuir variação, será o sku do produto.
     */
    @Id
    @GeneratedValue
    Long ID;
    /**
     * Identificador (sku) do produto.
     */
    String product_id;
    /**
     * Nome do produto
     */
    String name;
    /**
     * Quantidade
     */
    @Column(name = "quantidade")
    Integer qty;
    /**
     * Preço original
     */
    Float original_price;
    /**
     * Preço de venda
     */
    Float special_price;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Float getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(Float original_price) {
        this.original_price = original_price;
    }

    public Float getSpecial_price() {
        return special_price;
    }

    public void setSpecial_price(Float special_price) {
        this.special_price = special_price;
    }
}
