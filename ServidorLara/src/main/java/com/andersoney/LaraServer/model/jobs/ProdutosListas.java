/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.jobs;

import com.andersoney.LaraServer.model.Produto;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 *
 * @author ander
 */
@Entity
public class ProdutosListas extends ListJobs implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    Produto produto;

    public ProdutosListas() {
//        super();
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

}
