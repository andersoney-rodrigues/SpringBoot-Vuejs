/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.Skyhub;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Endereço de entrega. Alguns marketplaces só liberam o endereço de entrega do
 * pedido depois da confirmação de pagamento. Então para pedidos aguardando
 * confirmação de pagamento o campo endereço de entrega pode não aparecer.
 *
 * @author ander
 */
@Entity
public class SkyhubAddress implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;

    /**
     * Rua/Avenida
     */
    String street;
    /**
     * Número
     */
    @Column(name = "numero")
    String number;
    /**
     * Complemento
     */
    String detail;
    /**
     * Bairro
     */
    String neighborhood;
    /**
     * Cidade
     */
    String city;
    /**
     * Estado (sigla da UF)
     */
    String region;
    /**
     * País (código no formato ISO3166)
     */
    String country;
    /**
     * CEP
     */
    String postcode;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

}
