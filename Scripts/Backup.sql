PGDMP     7                     v            df3jqfmvegrebg    10.1    10.1 |    M           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            N           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            O           1262    6423626    df3jqfmvegrebg    DATABASE     �   CREATE DATABASE df3jqfmvegrebg WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE df3jqfmvegrebg;
             yvmqoumfgadihy    false                        2615    7276830    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             yvmqoumfgadihy    false                        3079    13809    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            P           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            Q           0    0    plpgsql    ACL     1   GRANT ALL ON LANGUAGE plpgsql TO yvmqoumfgadihy;
                  postgres    false    714            �            1259    7276853    atributo    TABLE     �   CREATE TABLE atributo (
    codatributo character varying(255) NOT NULL,
    userid bigint NOT NULL,
    dif boolean NOT NULL,
    name character varying(255)
);
    DROP TABLE public.atributo;
       public         yvmqoumfgadihy    false    6            �            1259    7276861    atributo_categorias    TABLE     �   CREATE TABLE atributo_categorias (
    atributo_codatributo character varying(255) NOT NULL,
    atributo_userid bigint NOT NULL,
    categorias_codecategory character varying(255) NOT NULL,
    categorias_userid bigint NOT NULL
);
 '   DROP TABLE public.atributo_categorias;
       public         yvmqoumfgadihy    false    6            �            1259    7276867    atributo_variacao    TABLE     �   CREATE TABLE atributo_variacao (
    atributo_codatributo character varying(255) NOT NULL,
    atributo_userid bigint NOT NULL,
    variacao character varying(255)
);
 %   DROP TABLE public.atributo_variacao;
       public         yvmqoumfgadihy    false    6            �            1259    7276873    banco    TABLE     �   CREATE TABLE banco (
    id bigint NOT NULL,
    agencia character varying(255),
    banco character varying(255),
    conta character varying(255),
    titular character varying(255)
);
    DROP TABLE public.banco;
       public         yvmqoumfgadihy    false    6            �            1259    7276881 	   categoria    TABLE     �   CREATE TABLE categoria (
    codecategory character varying(255) NOT NULL,
    userid bigint NOT NULL,
    name character varying(255)
);
    DROP TABLE public.categoria;
       public         yvmqoumfgadihy    false    6            �            1259    7276889    categoria_atributos    TABLE     �   CREATE TABLE categoria_atributos (
    categoria_codecategory character varying(255) NOT NULL,
    categoria_userid bigint NOT NULL,
    atributos_codatributo character varying(255) NOT NULL,
    atributos_userid bigint NOT NULL
);
 '   DROP TABLE public.categoria_atributos;
       public         yvmqoumfgadihy    false    6            �            1259    7276895    categorias_listas    TABLE     �   CREATE TABLE categorias_listas (
    id bigint NOT NULL,
    categoria_codecategory character varying(255),
    categoria_userid bigint
);
 %   DROP TABLE public.categorias_listas;
       public         yvmqoumfgadihy    false    6            �            1259    7276900    cliente    TABLE     �   CREATE TABLE cliente (
    cpf character varying(255),
    email character varying(255),
    name character varying(255),
    telefone character varying(255),
    id bigint NOT NULL,
    empresa_id bigint
);
    DROP TABLE public.cliente;
       public         yvmqoumfgadihy    false    6            �            1259    7276908    cliente_bancos    TABLE     _   CREATE TABLE cliente_bancos (
    cliente_id bigint NOT NULL,
    bancos_id bigint NOT NULL
);
 "   DROP TABLE public.cliente_bancos;
       public         yvmqoumfgadihy    false    6            �            1259    7276911    contato    TABLE     �   CREATE TABLE contato (
    id bigint NOT NULL,
    email character varying(255),
    nome character varying(255),
    telefone character varying(255)
);
    DROP TABLE public.contato;
       public         yvmqoumfgadihy    false    6            �            1259    7276919    empresa    TABLE     �  CREATE TABLE empresa (
    id bigint NOT NULL,
    cnpj character varying(255),
    bairro character varying(255),
    cadastro timestamp without time zone,
    celular character varying(255),
    cep character varying(255),
    cidade character varying(255),
    contato character varying(255),
    cpf character varying(255),
    data_fim timestamp without time zone,
    email character varying(255),
    endereco character varying(255),
    estado character varying(255),
    fantasia character varying(255),
    faturamento_anual character varying(255),
    inicio_ativacao timestamp without time zone,
    inicio_vendas timestamp without time zone,
    insc_estadual character varying(255),
    insc_municipal character varying(255),
    numero character varying(255),
    razao_social character varying(255),
    site character varying(255),
    telefone_fixo character varying(255),
    comercial_id bigint,
    financeiro_id bigint,
    operacoes_id bigint,
    ti_id bigint
);
    DROP TABLE public.empresa;
       public         yvmqoumfgadihy    false    6            �            1259    7276851    hibernate_sequence    SEQUENCE     t   CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.hibernate_sequence;
       public       yvmqoumfgadihy    false    6            �            1259    7276927 	   list_jobs    TABLE     �   CREATE TABLE list_jobs (
    id bigint NOT NULL,
    cadastro timestamp without time zone,
    json character varying(1024),
    operacoes integer,
    retorno timestamp without time zone,
    return_code integer,
    tipo integer
);
    DROP TABLE public.list_jobs;
       public         yvmqoumfgadihy    false    6            �            1259    7276935    price    TABLE     �   CREATE TABLE price (
    id bigint NOT NULL,
    brand character varying(255),
    cost real,
    price real,
    promotional_price real,
    return_code character varying(255)
);
    DROP TABLE public.price;
       public         yvmqoumfgadihy    false    6            �            1259    7276943    produto    TABLE     �  CREATE TABLE produto (
    id bigint NOT NULL,
    brand character varying(255),
    cost real,
    description character varying(255),
    ean character varying(255),
    height real,
    length real,
    name character varying(255),
    nbm character varying(255),
    produtc_code character varying(255),
    return_code character varying(255),
    status character varying(255),
    userid bigint,
    weight real
);
    DROP TABLE public.produto;
       public         yvmqoumfgadihy    false    6            �            1259    7276960    produto_atributo    TABLE     �   CREATE TABLE produto_atributo (
    id bigint NOT NULL,
    descri character varying(255),
    atributo_codatributo character varying(255),
    atributo_userid bigint
);
 $   DROP TABLE public.produto_atributo;
       public         yvmqoumfgadihy    false    6            �            1259    7276951    produto_imagens    TABLE     e   CREATE TABLE produto_imagens (
    produto_id bigint NOT NULL,
    imagens character varying(255)
);
 #   DROP TABLE public.produto_imagens;
       public         yvmqoumfgadihy    false    6            �            1259    7276954    produto_produto_atributo    TABLE     s   CREATE TABLE produto_produto_atributo (
    produto_id bigint NOT NULL,
    produto_atributo_id bigint NOT NULL
);
 ,   DROP TABLE public.produto_produto_atributo;
       public         yvmqoumfgadihy    false    6            �            1259    7276957    produto_skus    TABLE     [   CREATE TABLE produto_skus (
    produto_id bigint NOT NULL,
    skus_id bigint NOT NULL
);
     DROP TABLE public.produto_skus;
       public         yvmqoumfgadihy    false    6            �            1259    7276968    produtos_listas    TABLE     P   CREATE TABLE produtos_listas (
    id bigint NOT NULL,
    produto_id bigint
);
 #   DROP TABLE public.produtos_listas;
       public         yvmqoumfgadihy    false    6            �            1259    7276973    sku    TABLE     �   CREATE TABLE sku (
    id bigint NOT NULL,
    brand character varying(255),
    ean character varying(255),
    qty integer,
    return_code character varying(255),
    sku_code character varying(255)
);
    DROP TABLE public.sku;
       public         yvmqoumfgadihy    false    6            �            1259    7276981 
   sku_imagem    TABLE     [   CREATE TABLE sku_imagem (
    sku_id bigint NOT NULL,
    imagem character varying(255)
);
    DROP TABLE public.sku_imagem;
       public         yvmqoumfgadihy    false    6            �            1259    7276984 
   sku_prices    TABLE     W   CREATE TABLE sku_prices (
    sku_id bigint NOT NULL,
    prices_id bigint NOT NULL
);
    DROP TABLE public.sku_prices;
       public         yvmqoumfgadihy    false    6            �            1259    7276987 
   sku_stocks    TABLE     W   CREATE TABLE sku_stocks (
    sku_id bigint NOT NULL,
    stocks_id bigint NOT NULL
);
    DROP TABLE public.sku_stocks;
       public         yvmqoumfgadihy    false    6            �            1259    7276990    sku_variacoes    TABLE     ]   CREATE TABLE sku_variacoes (
    sku_id bigint NOT NULL,
    variacoes_id bigint NOT NULL
);
 !   DROP TABLE public.sku_variacoes;
       public         yvmqoumfgadihy    false    6            �            1259    7276993    stock    TABLE     �   CREATE TABLE stock (
    id bigint NOT NULL,
    brand character varying(255),
    qty integer,
    return_code character varying(255)
);
    DROP TABLE public.stock;
       public         yvmqoumfgadihy    false    6            �            1259    7277001    usuario    TABLE     {   CREATE TABLE usuario (
    id bigint NOT NULL,
    password character varying(255),
    username character varying(255)
);
    DROP TABLE public.usuario;
       public         yvmqoumfgadihy    false    6            �            1259    7277009    usuario_roles    TABLE     a   CREATE TABLE usuario_roles (
    usuario_id bigint NOT NULL,
    roles character varying(255)
);
 !   DROP TABLE public.usuario_roles;
       public         yvmqoumfgadihy    false    6            �            1259    7277012    variacao    TABLE     �   CREATE TABLE variacao (
    id bigint NOT NULL,
    descri character varying(255),
    atributo_codatributo character varying(255),
    atributo_userid bigint
);
    DROP TABLE public.variacao;
       public         yvmqoumfgadihy    false    6            /          0    7276853    atributo 
   TABLE DATA               ;   COPY atributo (codatributo, userid, dif, name) FROM stdin;
    public       yvmqoumfgadihy    false    197   ��       0          0    7276861    atributo_categorias 
   TABLE DATA               y   COPY atributo_categorias (atributo_codatributo, atributo_userid, categorias_codecategory, categorias_userid) FROM stdin;
    public       yvmqoumfgadihy    false    198   Ǟ       1          0    7276867    atributo_variacao 
   TABLE DATA               U   COPY atributo_variacao (atributo_codatributo, atributo_userid, variacao) FROM stdin;
    public       yvmqoumfgadihy    false    199   �       2          0    7276873    banco 
   TABLE DATA               <   COPY banco (id, agencia, banco, conta, titular) FROM stdin;
    public       yvmqoumfgadihy    false    200   �       3          0    7276881 	   categoria 
   TABLE DATA               8   COPY categoria (codecategory, userid, name) FROM stdin;
    public       yvmqoumfgadihy    false    201   �       4          0    7276889    categoria_atributos 
   TABLE DATA               y   COPY categoria_atributos (categoria_codecategory, categoria_userid, atributos_codatributo, atributos_userid) FROM stdin;
    public       yvmqoumfgadihy    false    202   ;�       5          0    7276895    categorias_listas 
   TABLE DATA               R   COPY categorias_listas (id, categoria_codecategory, categoria_userid) FROM stdin;
    public       yvmqoumfgadihy    false    203   X�       6          0    7276900    cliente 
   TABLE DATA               F   COPY cliente (cpf, email, name, telefone, id, empresa_id) FROM stdin;
    public       yvmqoumfgadihy    false    204   u�       7          0    7276908    cliente_bancos 
   TABLE DATA               8   COPY cliente_bancos (cliente_id, bancos_id) FROM stdin;
    public       yvmqoumfgadihy    false    205   ڟ       8          0    7276911    contato 
   TABLE DATA               5   COPY contato (id, email, nome, telefone) FROM stdin;
    public       yvmqoumfgadihy    false    206   ��       9          0    7276919    empresa 
   TABLE DATA               6  COPY empresa (id, cnpj, bairro, cadastro, celular, cep, cidade, contato, cpf, data_fim, email, endereco, estado, fantasia, faturamento_anual, inicio_ativacao, inicio_vendas, insc_estadual, insc_municipal, numero, razao_social, site, telefone_fixo, comercial_id, financeiro_id, operacoes_id, ti_id) FROM stdin;
    public       yvmqoumfgadihy    false    207   �       :          0    7276927 	   list_jobs 
   TABLE DATA               W   COPY list_jobs (id, cadastro, json, operacoes, retorno, return_code, tipo) FROM stdin;
    public       yvmqoumfgadihy    false    208   1�       ;          0    7276935    price 
   TABLE DATA               P   COPY price (id, brand, cost, price, promotional_price, return_code) FROM stdin;
    public       yvmqoumfgadihy    false    209   N�       <          0    7276943    produto 
   TABLE DATA               �   COPY produto (id, brand, cost, description, ean, height, length, name, nbm, produtc_code, return_code, status, userid, weight) FROM stdin;
    public       yvmqoumfgadihy    false    210   k�       @          0    7276960    produto_atributo 
   TABLE DATA               V   COPY produto_atributo (id, descri, atributo_codatributo, atributo_userid) FROM stdin;
    public       yvmqoumfgadihy    false    214   ��       =          0    7276951    produto_imagens 
   TABLE DATA               7   COPY produto_imagens (produto_id, imagens) FROM stdin;
    public       yvmqoumfgadihy    false    211   ��       >          0    7276954    produto_produto_atributo 
   TABLE DATA               L   COPY produto_produto_atributo (produto_id, produto_atributo_id) FROM stdin;
    public       yvmqoumfgadihy    false    212           ?          0    7276957    produto_skus 
   TABLE DATA               4   COPY produto_skus (produto_id, skus_id) FROM stdin;
    public       yvmqoumfgadihy    false    213   ߠ       A          0    7276968    produtos_listas 
   TABLE DATA               2   COPY produtos_listas (id, produto_id) FROM stdin;
    public       yvmqoumfgadihy    false    215   ��       B          0    7276973    sku 
   TABLE DATA               B   COPY sku (id, brand, ean, qty, return_code, sku_code) FROM stdin;
    public       yvmqoumfgadihy    false    216   �       C          0    7276981 
   sku_imagem 
   TABLE DATA               -   COPY sku_imagem (sku_id, imagem) FROM stdin;
    public       yvmqoumfgadihy    false    217   6�       D          0    7276984 
   sku_prices 
   TABLE DATA               0   COPY sku_prices (sku_id, prices_id) FROM stdin;
    public       yvmqoumfgadihy    false    218   S�       E          0    7276987 
   sku_stocks 
   TABLE DATA               0   COPY sku_stocks (sku_id, stocks_id) FROM stdin;
    public       yvmqoumfgadihy    false    219   p�       F          0    7276990    sku_variacoes 
   TABLE DATA               6   COPY sku_variacoes (sku_id, variacoes_id) FROM stdin;
    public       yvmqoumfgadihy    false    220   ��       G          0    7276993    stock 
   TABLE DATA               5   COPY stock (id, brand, qty, return_code) FROM stdin;
    public       yvmqoumfgadihy    false    221   ��       H          0    7277001    usuario 
   TABLE DATA               2   COPY usuario (id, password, username) FROM stdin;
    public       yvmqoumfgadihy    false    222   ǡ       I          0    7277009    usuario_roles 
   TABLE DATA               3   COPY usuario_roles (usuario_id, roles) FROM stdin;
    public       yvmqoumfgadihy    false    223   p�       J          0    7277012    variacao 
   TABLE DATA               N   COPY variacao (id, descri, atributo_codatributo, atributo_userid) FROM stdin;
    public       yvmqoumfgadihy    false    224   ��       R           0    0    hibernate_sequence    SEQUENCE SET     9   SELECT pg_catalog.setval('hibernate_sequence', 2, true);
            public       yvmqoumfgadihy    false    196            `           2606    7276860    atributo atributo_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY atributo
    ADD CONSTRAINT atributo_pkey PRIMARY KEY (codatributo, userid);
 @   ALTER TABLE ONLY public.atributo DROP CONSTRAINT atributo_pkey;
       public         yvmqoumfgadihy    false    197    197            b           2606    7276880    banco banco_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY banco
    ADD CONSTRAINT banco_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.banco DROP CONSTRAINT banco_pkey;
       public         yvmqoumfgadihy    false    200            d           2606    7276888    categoria categoria_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (codecategory, userid);
 B   ALTER TABLE ONLY public.categoria DROP CONSTRAINT categoria_pkey;
       public         yvmqoumfgadihy    false    201    201            f           2606    7276899 (   categorias_listas categorias_listas_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY categorias_listas
    ADD CONSTRAINT categorias_listas_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.categorias_listas DROP CONSTRAINT categorias_listas_pkey;
       public         yvmqoumfgadihy    false    203            h           2606    7276907    cliente cliente_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.cliente DROP CONSTRAINT cliente_pkey;
       public         yvmqoumfgadihy    false    204            n           2606    7276918    contato contato_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY contato
    ADD CONSTRAINT contato_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.contato DROP CONSTRAINT contato_pkey;
       public         yvmqoumfgadihy    false    206            p           2606    7276926    empresa empresa_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.empresa DROP CONSTRAINT empresa_pkey;
       public         yvmqoumfgadihy    false    207            v           2606    7276934    list_jobs list_jobs_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY list_jobs
    ADD CONSTRAINT list_jobs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.list_jobs DROP CONSTRAINT list_jobs_pkey;
       public         yvmqoumfgadihy    false    208            x           2606    7276942    price price_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY price
    ADD CONSTRAINT price_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.price DROP CONSTRAINT price_pkey;
       public         yvmqoumfgadihy    false    209            �           2606    7276967 &   produto_atributo produto_atributo_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY produto_atributo
    ADD CONSTRAINT produto_atributo_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.produto_atributo DROP CONSTRAINT produto_atributo_pkey;
       public         yvmqoumfgadihy    false    214            z           2606    7276950    produto produto_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY produto
    ADD CONSTRAINT produto_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.produto DROP CONSTRAINT produto_pkey;
       public         yvmqoumfgadihy    false    210            |           2606    7277029    produto produtocodeuseridunic 
   CONSTRAINT     a   ALTER TABLE ONLY produto
    ADD CONSTRAINT produtocodeuseridunic UNIQUE (userid, produtc_code);
 G   ALTER TABLE ONLY public.produto DROP CONSTRAINT produtocodeuseridunic;
       public         yvmqoumfgadihy    false    210    210            �           2606    7276972 $   produtos_listas produtos_listas_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY produtos_listas
    ADD CONSTRAINT produtos_listas_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.produtos_listas DROP CONSTRAINT produtos_listas_pkey;
       public         yvmqoumfgadihy    false    215            �           2606    7276980    sku sku_pkey 
   CONSTRAINT     C   ALTER TABLE ONLY sku
    ADD CONSTRAINT sku_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.sku DROP CONSTRAINT sku_pkey;
       public         yvmqoumfgadihy    false    216            �           2606    7277000    stock stock_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY stock
    ADD CONSTRAINT stock_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.stock DROP CONSTRAINT stock_pkey;
       public         yvmqoumfgadihy    false    221            �           2606    7277033 )   produto_skus uk_28douyabhqt7idn0xync8dbdv 
   CONSTRAINT     `   ALTER TABLE ONLY produto_skus
    ADD CONSTRAINT uk_28douyabhqt7idn0xync8dbdv UNIQUE (skus_id);
 S   ALTER TABLE ONLY public.produto_skus DROP CONSTRAINT uk_28douyabhqt7idn0xync8dbdv;
       public         yvmqoumfgadihy    false    213            r           2606    7277027 $   empresa uk_579mxohqloqo8pcqbpmeft807 
   CONSTRAINT     W   ALTER TABLE ONLY empresa
    ADD CONSTRAINT uk_579mxohqloqo8pcqbpmeft807 UNIQUE (cpf);
 N   ALTER TABLE ONLY public.empresa DROP CONSTRAINT uk_579mxohqloqo8pcqbpmeft807;
       public         yvmqoumfgadihy    false    207            t           2606    7277025 $   empresa uk_74xhe5obsc7li6x4q5wi75pd5 
   CONSTRAINT     X   ALTER TABLE ONLY empresa
    ADD CONSTRAINT uk_74xhe5obsc7li6x4q5wi75pd5 UNIQUE (cnpj);
 N   ALTER TABLE ONLY public.empresa DROP CONSTRAINT uk_74xhe5obsc7li6x4q5wi75pd5;
       public         yvmqoumfgadihy    false    207            �           2606    7277041 $   usuario uk_863n1y3x0jalatoir4325ehal 
   CONSTRAINT     \   ALTER TABLE ONLY usuario
    ADD CONSTRAINT uk_863n1y3x0jalatoir4325ehal UNIQUE (username);
 N   ALTER TABLE ONLY public.usuario DROP CONSTRAINT uk_863n1y3x0jalatoir4325ehal;
       public         yvmqoumfgadihy    false    222            l           2606    7277023 +   cliente_bancos uk_c2fubkfa8pvcuhfy6ry8esw8a 
   CONSTRAINT     d   ALTER TABLE ONLY cliente_bancos
    ADD CONSTRAINT uk_c2fubkfa8pvcuhfy6ry8esw8a UNIQUE (bancos_id);
 U   ALTER TABLE ONLY public.cliente_bancos DROP CONSTRAINT uk_c2fubkfa8pvcuhfy6ry8esw8a;
       public         yvmqoumfgadihy    false    205            �           2606    7277037 '   sku_stocks uk_gk5bigtckqcgeokcjid3qk1hr 
   CONSTRAINT     `   ALTER TABLE ONLY sku_stocks
    ADD CONSTRAINT uk_gk5bigtckqcgeokcjid3qk1hr UNIQUE (stocks_id);
 Q   ALTER TABLE ONLY public.sku_stocks DROP CONSTRAINT uk_gk5bigtckqcgeokcjid3qk1hr;
       public         yvmqoumfgadihy    false    219            �           2606    7277035 '   sku_prices uk_h6jguc9ieq5896qc001vlvhao 
   CONSTRAINT     `   ALTER TABLE ONLY sku_prices
    ADD CONSTRAINT uk_h6jguc9ieq5896qc001vlvhao UNIQUE (prices_id);
 Q   ALTER TABLE ONLY public.sku_prices DROP CONSTRAINT uk_h6jguc9ieq5896qc001vlvhao;
       public         yvmqoumfgadihy    false    218            �           2606    7277039 *   sku_variacoes uk_mnyq60u1huw30apqwd19dugdn 
   CONSTRAINT     f   ALTER TABLE ONLY sku_variacoes
    ADD CONSTRAINT uk_mnyq60u1huw30apqwd19dugdn UNIQUE (variacoes_id);
 T   ALTER TABLE ONLY public.sku_variacoes DROP CONSTRAINT uk_mnyq60u1huw30apqwd19dugdn;
       public         yvmqoumfgadihy    false    220            ~           2606    7277031 5   produto_produto_atributo uk_q1c149mn3tme19i3kxtokqcc6 
   CONSTRAINT     x   ALTER TABLE ONLY produto_produto_atributo
    ADD CONSTRAINT uk_q1c149mn3tme19i3kxtokqcc6 UNIQUE (produto_atributo_id);
 _   ALTER TABLE ONLY public.produto_produto_atributo DROP CONSTRAINT uk_q1c149mn3tme19i3kxtokqcc6;
       public         yvmqoumfgadihy    false    212            j           2606    7277021 $   cliente uk_r1u8010d60num5vc8fp0q1j2a 
   CONSTRAINT     W   ALTER TABLE ONLY cliente
    ADD CONSTRAINT uk_r1u8010d60num5vc8fp0q1j2a UNIQUE (cpf);
 N   ALTER TABLE ONLY public.cliente DROP CONSTRAINT uk_r1u8010d60num5vc8fp0q1j2a;
       public         yvmqoumfgadihy    false    204            �           2606    7277008    usuario usuario_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public         yvmqoumfgadihy    false    222            �           2606    7277019    variacao variacao_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY variacao
    ADD CONSTRAINT variacao_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.variacao DROP CONSTRAINT variacao_pkey;
       public         yvmqoumfgadihy    false    224            �           2606    7277072 -   categorias_listas fk1exgld9e7gjdc7r7o97v0aixg    FK CONSTRAINT     }   ALTER TABLE ONLY categorias_listas
    ADD CONSTRAINT fk1exgld9e7gjdc7r7o97v0aixg FOREIGN KEY (id) REFERENCES list_jobs(id);
 W   ALTER TABLE ONLY public.categorias_listas DROP CONSTRAINT fk1exgld9e7gjdc7r7o97v0aixg;
       public       yvmqoumfgadihy    false    208    203    3702            �           2606    7277182 )   sku_variacoes fk1h538a1vmoos38bkjnqx0d5ix    FK CONSTRAINT     �   ALTER TABLE ONLY sku_variacoes
    ADD CONSTRAINT fk1h538a1vmoos38bkjnqx0d5ix FOREIGN KEY (variacoes_id) REFERENCES variacao(id);
 S   ALTER TABLE ONLY public.sku_variacoes DROP CONSTRAINT fk1h538a1vmoos38bkjnqx0d5ix;
       public       yvmqoumfgadihy    false    3732    220    224            �           2606    7277127 4   produto_produto_atributo fk1mno72qc42qd12pnu1gf9c9uv    FK CONSTRAINT     �   ALTER TABLE ONLY produto_produto_atributo
    ADD CONSTRAINT fk1mno72qc42qd12pnu1gf9c9uv FOREIGN KEY (produto_id) REFERENCES produto(id);
 ^   ALTER TABLE ONLY public.produto_produto_atributo DROP CONSTRAINT fk1mno72qc42qd12pnu1gf9c9uv;
       public       yvmqoumfgadihy    false    3706    210    212            �           2606    7277177 &   sku_stocks fk2d1toc97p18dpokxd08hi0mpq    FK CONSTRAINT     t   ALTER TABLE ONLY sku_stocks
    ADD CONSTRAINT fk2d1toc97p18dpokxd08hi0mpq FOREIGN KEY (sku_id) REFERENCES sku(id);
 P   ALTER TABLE ONLY public.sku_stocks DROP CONSTRAINT fk2d1toc97p18dpokxd08hi0mpq;
       public       yvmqoumfgadihy    false    216    219    3718            �           2606    7277157 &   sku_imagem fk3c36mx6ikcvm0j3mwrndr7b8a    FK CONSTRAINT     t   ALTER TABLE ONLY sku_imagem
    ADD CONSTRAINT fk3c36mx6ikcvm0j3mwrndr7b8a FOREIGN KEY (sku_id) REFERENCES sku(id);
 P   ALTER TABLE ONLY public.sku_imagem DROP CONSTRAINT fk3c36mx6ikcvm0j3mwrndr7b8a;
       public       yvmqoumfgadihy    false    217    3718    216            �           2606    7277162 &   sku_prices fk47a49ucgch6f51fvs1kbdrl71    FK CONSTRAINT     y   ALTER TABLE ONLY sku_prices
    ADD CONSTRAINT fk47a49ucgch6f51fvs1kbdrl71 FOREIGN KEY (prices_id) REFERENCES price(id);
 P   ALTER TABLE ONLY public.sku_prices DROP CONSTRAINT fk47a49ucgch6f51fvs1kbdrl71;
       public       yvmqoumfgadihy    false    209    218    3704            �           2606    7277052 -   atributo_variacao fk660tf2e3j18sxogcck7hpbowc    FK CONSTRAINT     �   ALTER TABLE ONLY atributo_variacao
    ADD CONSTRAINT fk660tf2e3j18sxogcck7hpbowc FOREIGN KEY (atributo_codatributo, atributo_userid) REFERENCES atributo(codatributo, userid);
 W   ALTER TABLE ONLY public.atributo_variacao DROP CONSTRAINT fk660tf2e3j18sxogcck7hpbowc;
       public       yvmqoumfgadihy    false    197    3680    199    199    197            �           2606    7277122 4   produto_produto_atributo fk6a62c6g0pxfalf6ubuwjtwbbp    FK CONSTRAINT     �   ALTER TABLE ONLY produto_produto_atributo
    ADD CONSTRAINT fk6a62c6g0pxfalf6ubuwjtwbbp FOREIGN KEY (produto_atributo_id) REFERENCES produto_atributo(id);
 ^   ALTER TABLE ONLY public.produto_produto_atributo DROP CONSTRAINT fk6a62c6g0pxfalf6ubuwjtwbbp;
       public       yvmqoumfgadihy    false    3714    214    212            �           2606    7277147 +   produtos_listas fk6bt8jhxojjsxkb1gd9ffefvib    FK CONSTRAINT     �   ALTER TABLE ONLY produtos_listas
    ADD CONSTRAINT fk6bt8jhxojjsxkb1gd9ffefvib FOREIGN KEY (produto_id) REFERENCES produto(id);
 U   ALTER TABLE ONLY public.produtos_listas DROP CONSTRAINT fk6bt8jhxojjsxkb1gd9ffefvib;
       public       yvmqoumfgadihy    false    210    3706    215            �           2606    7277042 /   atributo_categorias fk6yj63avlkt625lkiec89g8260    FK CONSTRAINT     �   ALTER TABLE ONLY atributo_categorias
    ADD CONSTRAINT fk6yj63avlkt625lkiec89g8260 FOREIGN KEY (categorias_codecategory, categorias_userid) REFERENCES categoria(codecategory, userid);
 Y   ALTER TABLE ONLY public.atributo_categorias DROP CONSTRAINT fk6yj63avlkt625lkiec89g8260;
       public       yvmqoumfgadihy    false    198    198    3684    201    201            �           2606    7277112 #   empresa fk7odfqe0hb6qt4k5jpeqp45ss1    FK CONSTRAINT     t   ALTER TABLE ONLY empresa
    ADD CONSTRAINT fk7odfqe0hb6qt4k5jpeqp45ss1 FOREIGN KEY (ti_id) REFERENCES contato(id);
 M   ALTER TABLE ONLY public.empresa DROP CONSTRAINT fk7odfqe0hb6qt4k5jpeqp45ss1;
       public       yvmqoumfgadihy    false    3694    206    207            �           2606    7277062 /   categoria_atributos fk8jgxkecnx7a1tykk9x7pq4u1t    FK CONSTRAINT     �   ALTER TABLE ONLY categoria_atributos
    ADD CONSTRAINT fk8jgxkecnx7a1tykk9x7pq4u1t FOREIGN KEY (categoria_codecategory, categoria_userid) REFERENCES categoria(codecategory, userid);
 Y   ALTER TABLE ONLY public.categoria_atributos DROP CONSTRAINT fk8jgxkecnx7a1tykk9x7pq4u1t;
       public       yvmqoumfgadihy    false    202    202    201    201    3684            �           2606    7277097 #   empresa fkamov647pbxmkgter6y3ivsdl7    FK CONSTRAINT     {   ALTER TABLE ONLY empresa
    ADD CONSTRAINT fkamov647pbxmkgter6y3ivsdl7 FOREIGN KEY (comercial_id) REFERENCES contato(id);
 M   ALTER TABLE ONLY public.empresa DROP CONSTRAINT fkamov647pbxmkgter6y3ivsdl7;
       public       yvmqoumfgadihy    false    207    3694    206            �           2606    7277057 /   categoria_atributos fkbqntbo91t3pmc59busv2n516u    FK CONSTRAINT     �   ALTER TABLE ONLY categoria_atributos
    ADD CONSTRAINT fkbqntbo91t3pmc59busv2n516u FOREIGN KEY (atributos_codatributo, atributos_userid) REFERENCES atributo(codatributo, userid);
 Y   ALTER TABLE ONLY public.categoria_atributos DROP CONSTRAINT fkbqntbo91t3pmc59busv2n516u;
       public       yvmqoumfgadihy    false    202    202    197    197    3680            �           2606    7277132 (   produto_skus fkdaxo6ddw8eegk92xgg4p9kwby    FK CONSTRAINT     w   ALTER TABLE ONLY produto_skus
    ADD CONSTRAINT fkdaxo6ddw8eegk92xgg4p9kwby FOREIGN KEY (skus_id) REFERENCES sku(id);
 R   ALTER TABLE ONLY public.produto_skus DROP CONSTRAINT fkdaxo6ddw8eegk92xgg4p9kwby;
       public       yvmqoumfgadihy    false    3718    216    213            �           2606    7277152 +   produtos_listas fkf70igfoufts4dm5vtxg720hx5    FK CONSTRAINT     {   ALTER TABLE ONLY produtos_listas
    ADD CONSTRAINT fkf70igfoufts4dm5vtxg720hx5 FOREIGN KEY (id) REFERENCES list_jobs(id);
 U   ALTER TABLE ONLY public.produtos_listas DROP CONSTRAINT fkf70igfoufts4dm5vtxg720hx5;
       public       yvmqoumfgadihy    false    215    208    3702            �           2606    7277187 )   sku_variacoes fkg6wi90gocojf99qow5b6h7seu    FK CONSTRAINT     w   ALTER TABLE ONLY sku_variacoes
    ADD CONSTRAINT fkg6wi90gocojf99qow5b6h7seu FOREIGN KEY (sku_id) REFERENCES sku(id);
 S   ALTER TABLE ONLY public.sku_variacoes DROP CONSTRAINT fkg6wi90gocojf99qow5b6h7seu;
       public       yvmqoumfgadihy    false    216    220    3718            �           2606    7277117 +   produto_imagens fkg8laoe7wod2vuf03jn914tuli    FK CONSTRAINT     �   ALTER TABLE ONLY produto_imagens
    ADD CONSTRAINT fkg8laoe7wod2vuf03jn914tuli FOREIGN KEY (produto_id) REFERENCES produto(id);
 U   ALTER TABLE ONLY public.produto_imagens DROP CONSTRAINT fkg8laoe7wod2vuf03jn914tuli;
       public       yvmqoumfgadihy    false    3706    210    211            �           2606    7277172 &   sku_stocks fkgb79apjt994qisk38tu0a1fvg    FK CONSTRAINT     y   ALTER TABLE ONLY sku_stocks
    ADD CONSTRAINT fkgb79apjt994qisk38tu0a1fvg FOREIGN KEY (stocks_id) REFERENCES stock(id);
 P   ALTER TABLE ONLY public.sku_stocks DROP CONSTRAINT fkgb79apjt994qisk38tu0a1fvg;
       public       yvmqoumfgadihy    false    3726    219    221            �           2606    7277137 (   produto_skus fkhvxqv303ipoqwoduiwkp2uc2m    FK CONSTRAINT     ~   ALTER TABLE ONLY produto_skus
    ADD CONSTRAINT fkhvxqv303ipoqwoduiwkp2uc2m FOREIGN KEY (produto_id) REFERENCES produto(id);
 R   ALTER TABLE ONLY public.produto_skus DROP CONSTRAINT fkhvxqv303ipoqwoduiwkp2uc2m;
       public       yvmqoumfgadihy    false    3706    210    213            �           2606    7277197 $   variacao fkj5em2mvhqsy3oagh6umygp3q2    FK CONSTRAINT     �   ALTER TABLE ONLY variacao
    ADD CONSTRAINT fkj5em2mvhqsy3oagh6umygp3q2 FOREIGN KEY (atributo_codatributo, atributo_userid) REFERENCES atributo(codatributo, userid);
 N   ALTER TABLE ONLY public.variacao DROP CONSTRAINT fkj5em2mvhqsy3oagh6umygp3q2;
       public       yvmqoumfgadihy    false    197    224    224    197    3680            �           2606    7277077 #   cliente fkkbui05oidjdj4nb0283u4t319    FK CONSTRAINT     y   ALTER TABLE ONLY cliente
    ADD CONSTRAINT fkkbui05oidjdj4nb0283u4t319 FOREIGN KEY (empresa_id) REFERENCES empresa(id);
 M   ALTER TABLE ONLY public.cliente DROP CONSTRAINT fkkbui05oidjdj4nb0283u4t319;
       public       yvmqoumfgadihy    false    3696    204    207            �           2606    7277167 &   sku_prices fkml5dnmcaq4a6ft2lodfj0xgc1    FK CONSTRAINT     t   ALTER TABLE ONLY sku_prices
    ADD CONSTRAINT fkml5dnmcaq4a6ft2lodfj0xgc1 FOREIGN KEY (sku_id) REFERENCES sku(id);
 P   ALTER TABLE ONLY public.sku_prices DROP CONSTRAINT fkml5dnmcaq4a6ft2lodfj0xgc1;
       public       yvmqoumfgadihy    false    218    216    3718            �           2606    7277047 /   atributo_categorias fko1iv2lbi8s0776xu98atha1h6    FK CONSTRAINT     �   ALTER TABLE ONLY atributo_categorias
    ADD CONSTRAINT fko1iv2lbi8s0776xu98atha1h6 FOREIGN KEY (atributo_codatributo, atributo_userid) REFERENCES atributo(codatributo, userid);
 Y   ALTER TABLE ONLY public.atributo_categorias DROP CONSTRAINT fko1iv2lbi8s0776xu98atha1h6;
       public       yvmqoumfgadihy    false    3680    198    198    197    197            �           2606    7277067 -   categorias_listas fkoo6mgdv81ro7k4ie0opuncnga    FK CONSTRAINT     �   ALTER TABLE ONLY categorias_listas
    ADD CONSTRAINT fkoo6mgdv81ro7k4ie0opuncnga FOREIGN KEY (categoria_codecategory, categoria_userid) REFERENCES categoria(codecategory, userid);
 W   ALTER TABLE ONLY public.categorias_listas DROP CONSTRAINT fkoo6mgdv81ro7k4ie0opuncnga;
       public       yvmqoumfgadihy    false    203    3684    201    201    203            �           2606    7277142 ,   produto_atributo fkoqpjtqsingtqyb9fyoyplfcnm    FK CONSTRAINT     �   ALTER TABLE ONLY produto_atributo
    ADD CONSTRAINT fkoqpjtqsingtqyb9fyoyplfcnm FOREIGN KEY (atributo_codatributo, atributo_userid) REFERENCES atributo(codatributo, userid);
 V   ALTER TABLE ONLY public.produto_atributo DROP CONSTRAINT fkoqpjtqsingtqyb9fyoyplfcnm;
       public       yvmqoumfgadihy    false    197    197    214    3680    214            �           2606    7277192 )   usuario_roles fkqblnumndby0ftm4c7sg6uso6p    FK CONSTRAINT        ALTER TABLE ONLY usuario_roles
    ADD CONSTRAINT fkqblnumndby0ftm4c7sg6uso6p FOREIGN KEY (usuario_id) REFERENCES usuario(id);
 S   ALTER TABLE ONLY public.usuario_roles DROP CONSTRAINT fkqblnumndby0ftm4c7sg6uso6p;
       public       yvmqoumfgadihy    false    222    223    3730            �           2606    7277092 *   cliente_bancos fkqn4mpnjwk68cfd5ap6lr1q3n7    FK CONSTRAINT     �   ALTER TABLE ONLY cliente_bancos
    ADD CONSTRAINT fkqn4mpnjwk68cfd5ap6lr1q3n7 FOREIGN KEY (cliente_id) REFERENCES cliente(id);
 T   ALTER TABLE ONLY public.cliente_bancos DROP CONSTRAINT fkqn4mpnjwk68cfd5ap6lr1q3n7;
       public       yvmqoumfgadihy    false    3688    204    205            �           2606    7277107 #   empresa fkrwtfyv28t8eme3u7r26n5qkca    FK CONSTRAINT     {   ALTER TABLE ONLY empresa
    ADD CONSTRAINT fkrwtfyv28t8eme3u7r26n5qkca FOREIGN KEY (operacoes_id) REFERENCES contato(id);
 M   ALTER TABLE ONLY public.empresa DROP CONSTRAINT fkrwtfyv28t8eme3u7r26n5qkca;
       public       yvmqoumfgadihy    false    207    3694    206            �           2606    7277082 #   cliente fksitxst8o302fspskxfjatuyrl    FK CONSTRAINT     q   ALTER TABLE ONLY cliente
    ADD CONSTRAINT fksitxst8o302fspskxfjatuyrl FOREIGN KEY (id) REFERENCES usuario(id);
 M   ALTER TABLE ONLY public.cliente DROP CONSTRAINT fksitxst8o302fspskxfjatuyrl;
       public       yvmqoumfgadihy    false    204    3730    222            �           2606    7277102 #   empresa fksloaigfwf3t98ltltojshrdsb    FK CONSTRAINT     |   ALTER TABLE ONLY empresa
    ADD CONSTRAINT fksloaigfwf3t98ltltojshrdsb FOREIGN KEY (financeiro_id) REFERENCES contato(id);
 M   ALTER TABLE ONLY public.empresa DROP CONSTRAINT fksloaigfwf3t98ltltojshrdsb;
       public       yvmqoumfgadihy    false    3694    206    207            �           2606    7277087 *   cliente_bancos fkspmyic64i52bdt8ko6rtrqvgr    FK CONSTRAINT     }   ALTER TABLE ONLY cliente_bancos
    ADD CONSTRAINT fkspmyic64i52bdt8ko6rtrqvgr FOREIGN KEY (bancos_id) REFERENCES banco(id);
 T   ALTER TABLE ONLY public.cliente_bancos DROP CONSTRAINT fkspmyic64i52bdt8ko6rtrqvgr;
       public       yvmqoumfgadihy    false    205    3682    200            /      x������ � �      0      x������ � �      1      x������ � �      2      x������ � �      3      x������ � �      4      x������ � �      5      x������ � �      6   U   x�3057144555��L�KI-*��K�tH�M���K���t�*��+'� ��������bNsSKK#C#SSN#�?�=... ��      7      x������ � �      8      x������ � �      9      x������ � �      :      x������ � �      ;      x������ � �      <      x������ � �      @      x������ � �      =      x������ � �      >      x������ � �      ?      x������ � �      A      x������ � �      B      x������ � �      C      x������ � �      D      x������ � �      E      x������ � �      F      x������ � �      G      x������ � �      H   �   x�5�K�0 �5=k"$n-~HA%m��WZbS-�G�ӻr0�e�`{��<f��uٸ_~.7l.b�Q��i�)�lP	T���r7-d=dl�x)����t��g���*����I��j�����;��[�ڂ�Q	jꩅ=O�X`��������-�2h      I   $   x�3�v�2�tt��2�t��t�q����� T`�      J      x������ � �     