package com.andersoney.LaraServer;

import com.andersoney.LaraServer.annotations.Definition;
import com.andersoney.LaraServer.config.Urls.UrlsSkyhub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication(scanBasePackages = "com.andersoney.LaraServer")
@EnableScheduling
@Definition()
@EntityScan(basePackages = {"com.andersoney.LaraServer.model"})
@EnableJpaRepositories(basePackages = "com.andersoney.LaraServer.repository")
public class SpringBootWebApplication {

    private static final Logger log = LoggerFactory.getLogger(SpringBootWebApplication.class);

    UrlsSkyhub urlsSkyhub;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SpringBootWebApplication.class, args);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Bean
    public UrlsSkyhub urlsSkyhub() {
        if (urlsSkyhub == null) {
            urlsSkyhub = new UrlsSkyhub();
        }
        return urlsSkyhub;
    }

}
