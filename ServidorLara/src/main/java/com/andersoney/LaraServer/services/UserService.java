/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.andersoney.LaraServer.repository.UsuarioRepository;

/**
 *
 * @author ander
 */
@Service
public class UserService {

    private final static Logger log = LoggerFactory.getLogger(UserService.class);
    
    @Autowired
    private static UsuarioRepository appUserRepository;

    @Transactional
    public static Long getUserId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        log.info("Usuario logado : " + loggedUsername);
        log.trace("" + (appUserRepository == null));
        log.trace("Usuario Logado" + appUserRepository.findOneByUsername(loggedUsername));
        Long id = appUserRepository.findOneByUsername(loggedUsername).getId();
        return id;
    }

    @Transactional
    public static Long getUserIdAdm() {
        Long id = appUserRepository.findOneByUsername("administrador").getId();
        return id;
    }

    @Autowired
    public void setAppUserRepository(UsuarioRepository appUserRepository) {
        UserService.appUserRepository = appUserRepository;
    }
}
