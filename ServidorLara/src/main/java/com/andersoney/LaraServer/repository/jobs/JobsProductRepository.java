/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.repository.jobs;

import com.andersoney.LaraServer.model.jobs.ProdutosListas;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ander
 */
public interface JobsProductRepository  extends JpaRepository<ProdutosListas, Long>{
    
}
