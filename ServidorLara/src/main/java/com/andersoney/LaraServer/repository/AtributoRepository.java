/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.repository;

import com.andersoney.LaraServer.model.Atributo;
import com.andersoney.LaraServer.model.ids.AtributoId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author ander
 */
public interface AtributoRepository extends JpaRepository<Atributo, AtributoId> {

    @Query(value = "SELECT a FROM Atributo a where a.userid = ?1",
            countQuery = "SELECT count(*) FROM Atributo",
            countName = "com.andersoney.LaraServer.model.Atributo.${queryMethodName}}")
    Page<Atributo> findAll(long userid, Pageable pageable);

}
