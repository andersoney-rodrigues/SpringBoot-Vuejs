/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.config.Urls;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author ander
 */
@ConfigurationProperties(prefix = "com.andersoney.LaraServer.urls")
@Configuration("urlConfigures")
public class URLConfigures {

    private static String skyhub;

    public String getSkyhub() {
        return skyhub;
    }

    public void setSkyhub(String skyhub) {
        this.skyhub = skyhub;
    }

}
