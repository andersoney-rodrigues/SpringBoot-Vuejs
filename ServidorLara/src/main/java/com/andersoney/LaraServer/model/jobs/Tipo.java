/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.jobs;

/**
 *
 * @author ander
 */
public enum Tipo {
    CATEGORIA(1),
    PRODUTO(2);

    private final Integer tipo;

    Tipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getTipo() {
        return tipo;
    }

}
