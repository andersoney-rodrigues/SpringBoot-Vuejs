/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.config.Urls;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ander
 */
public class UrlsSkyhub {

    @Autowired
    private URLConfigures urlConfigures;

    private final String attributes = "";
    private final String categories = "";
    private final String freights = "";
    private final String orders = "";
    private final String Postagem = "";
    private final String products = "";
    private final String queues = "";
    private final String SAC = "";
    private final String sale_systems = "";
    private final String statuses = "";
    private final String status_types = "";
    private final String sync_errors = "";
    private final String variations = "";

    public String getAttributes() {
        return urlConfigures.getSkyhub() + attributes;
    }

    public String getCategories() {
        return urlConfigures.getSkyhub() + categories;
    }

    public String getFreights() {
        return urlConfigures.getSkyhub() + freights;
    }

    public String getOrders() {
        return urlConfigures.getSkyhub() + orders;
    }

    public String getPostagem() {
        return urlConfigures.getSkyhub() + Postagem;
    }

    public String getProducts() {
        return urlConfigures.getSkyhub() + products;
    }

    public String getQueues() {
        return urlConfigures.getSkyhub() + queues;
    }

    public String getSAC() {
        return urlConfigures.getSkyhub() + SAC;
    }

    public String getSale_systems() {
        return urlConfigures.getSkyhub() + sale_systems;
    }

    public String getStatuses() {
        return urlConfigures.getSkyhub() + statuses;
    }

    public String getStatus_types() {
        return urlConfigures.getSkyhub() + status_types;
    }

    public String getSync_errors() {
        return urlConfigures.getSkyhub() + sync_errors;
    }

    public String getVariations() {
        return urlConfigures.getSkyhub() + variations;
    }

}
