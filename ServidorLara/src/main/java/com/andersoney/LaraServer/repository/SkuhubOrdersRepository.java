/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.repository;

import com.andersoney.LaraServer.model.Skyhub.SkyhubOrder;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ander
 */
public interface SkuhubOrdersRepository extends JpaRepository<SkyhubOrder, Long> {

    public List<SkyhubOrder> findByUserid(Long userid);

    public Page<SkyhubOrder> findByUserid(Long userid, Pageable pageable);
}
