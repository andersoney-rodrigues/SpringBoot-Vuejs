/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.services;

import com.andersoney.LaraServer.model.Atributo;
import com.andersoney.LaraServer.model.ids.AtributoId;
import com.andersoney.LaraServer.repository.AtributoRepository;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author ander
 */
@Service
public class AtributoService {

    @Autowired
    AtributoRepository atributoRepository;

    @Autowired
    UserService userService;
    private static final Logger log = LoggerFactory.getLogger(AtributoService.class);

    public Atributo cadastrar(Atributo object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Page<Atributo> obterAllPageable(Pageable pageable) {
        return atributoRepository.findAll(userService.getUserId(), pageable);
    }

    public Atributo obter(AtributoId id) {
        return atributoRepository.findOne(id);
    }

    public Atributo atualizar(AtributoId id, Atributo produto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Atributo excluir(AtributoId id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Atributo> obterTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
