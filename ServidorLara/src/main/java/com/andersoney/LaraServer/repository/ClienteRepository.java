/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.repository;

import com.andersoney.LaraServer.model.System.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ander
 */
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    public Cliente findOneByUsername(String username);
}
