/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.services;

import java.security.Key;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.andersoney.LaraServer.repository.UsuarioRepository;

/**
 *
 * @author ander
 */
@Service
public abstract class DefaultService<T, S> {

    private static final Logger log = LoggerFactory.getLogger(DefaultService.class);
    @Autowired
    protected UsuarioRepository appUserRepository;

    /**
     *
     * @param object
     * @return
     */
    @Transactional
    abstract public T cadastrar(T object);

    /**
     *
     * @param pageable
     * @return
     */
    @Transactional
    abstract public Page<T> obterAllPageable(Long userid,Pageable pageable);

    @Transactional(readOnly = true)
    abstract public T obter(S id);

    @Transactional
    abstract public T atualizar(S id, T produto);

    @Transactional
    abstract public T excluir(S id);

    @Transactional(readOnly = true)
    abstract public List<T> obterTodos();
}
