/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.services;

import com.andersoney.LaraServer.model.System.Banco;
import com.andersoney.LaraServer.model.System.Cliente;
import com.andersoney.LaraServer.model.System.Empresa;
import com.andersoney.LaraServer.model.System.Usuario;
import com.andersoney.LaraServer.repository.ClienteRepository;
import com.andersoney.LaraServer.repository.UsuarioRepository;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ander
 */
@Service
public class ClienteService {
    
    private final Logger log = LoggerFactory.getLogger(ClienteService.class);
    
    @Autowired
    private UsuarioRepository appUserRepository;
    
    @Autowired
    ClienteRepository clienteRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Transactional
    public Cliente cadastrar(Cliente user) {
        if (clienteRepository.findOneByUsername(user.getUsername()) != null) {
            throw new RuntimeException("Username already exist");
        }
        List<String> roles = new ArrayList<>();
        roles.add("CLIENTE");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(roles);
        Gson gson = new Gson();
        log.info("Usuario: \n" + gson.toJson(user));
        return clienteRepository.save(user);
    }
    
    @Transactional
    public Page<Cliente> obterAllPageable(Long userid, Pageable pageable) {
        return null;
    }
    
    @Transactional
    public Cliente obter(Long id) {
        return null;
    }
    
    @Transactional
    public Cliente getCliente() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        return clienteRepository.findOneByUsername(loggedUsername);
    }
    
    @Transactional
    public Usuario getUsuario() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        return appUserRepository.findOneByUsername(loggedUsername);
    }
    
    @Transactional
    public Cliente atualizar(Long id, Cliente produto) {
        return null;
    }
    
    @Transactional
    public Cliente atualizar(Cliente cliente) {
        Long userid = UserService.getUserId();
        Cliente clienteA = clienteRepository.findOne(userid);
        if (cliente.getName() != null) {
            clienteA.setName(cliente.getName());
        }
        if (cliente.getTelefone() != null) {
            clienteA.setTelefone(cliente.getTelefone());
        }
        if (cliente.getEmail() != null) {
            clienteA.setEmail(cliente.getEmail());
        }
        return clienteRepository.save(clienteA);
    }
    
    @Transactional
    public Banco getBanco() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        Cliente cliente = clienteRepository.findOneByUsername(loggedUsername);
        if (cliente.getBancos().size() == 0) {
            return new Banco();
        } else {
            return cliente.getBancos().get(0);
        }
    }
    
    @Transactional
    public Banco atualizarBanco(Banco banco) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        Cliente cliente = clienteRepository.findOneByUsername(loggedUsername);
        if (cliente.getBancos() == null) {
            cliente.setBancos(new ArrayList<Banco>());
            cliente.getBancos().add(banco);
        } else if (cliente.getBancos().size() == 0) {
            cliente.getBancos().add(banco);
        } else {
            Banco bancoA = cliente.getBancos().get(0);
            bancoA.setAgencia(banco.getAgencia());
            bancoA.setBanco(banco.getBanco());
            bancoA.setConta(banco.getConta());
            bancoA.setTitular(banco.getTitular());
            cliente.getBancos().set(0, bancoA);
        }
        Cliente clienteF = clienteRepository.save(cliente);
        return clienteF.getBancos().get(0);
    }
    
    @Transactional
    public Empresa getEmpresa() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        Cliente cliente = clienteRepository.findOneByUsername(loggedUsername);
        Empresa empresa = cliente.getEmpresa();
        if (empresa == null) {
            return new Empresa();
        } else {
            return empresa;
        }
    }
    
    @Transactional
    public Empresa atualizarEmpresa(Empresa empresa) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        Cliente cliente = clienteRepository.findOneByUsername(loggedUsername);
        Empresa empre = cliente.getEmpresa();
        if (empre == null) {
            cliente.setEmpresa(empresa);
            cliente = clienteRepository.saveAndFlush(cliente);
            return cliente.getEmpresa();
        } else {
            empre.setCNPJ(empresa.getCNPJ());
            empre.setRazaoSocial(empresa.getRazaoSocial());
            empre.setFantasia(empresa.getFantasia());
            empre.setInscMunicipal(empresa.getInscMunicipal());
            empre.setInscEstadual(empresa.getInscEstadual());
            empre.setSite(empresa.getSite());
            empre.setFaturamentoAnual(empresa.getFaturamentoAnual());
            empre.setEndereco(empresa.getEndereco());
            empre.setNumero(empresa.getNumero());
            empre.setBairro(empresa.getBairro());
            empre.setCep(empresa.getCep());
            empre.setCidade(empresa.getCidade());
            empre.setEstado(empresa.getEstado());
            empre.setContato(empresa.getContato());
            empre.setCpf(empresa.getCpf());
            empre.setEmail(empresa.getEmail());
            empre.setTelefoneFixo(empresa.getTelefoneFixo());
            empre.setCelular(empresa.getCelular());
            empre.setCadastro(empresa.getCadastro());
            empre.setInicioAtivacao(empresa.getInicioAtivacao());
            empre.setInicioVendas(empresa.getInicioVendas());
            empre.setDataFim(empresa.getDataFim());
            empre.setComercial(empresa.getComercial());
            empre.setFinanceiro(empresa.getFinanceiro());
            empre.setOperacoes(empresa.getOperacoes());
            empre.setTi(empresa.getTi());
            cliente.setEmpresa(empre);
            cliente = clienteRepository.save(cliente);
            log.debug("Cliente:\n" + new Gson().toJson(cliente));
            return cliente.getEmpresa();
        }
    }
    
    @Transactional
    public Cliente excluir(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Transactional
    public List<Cliente> obterTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
