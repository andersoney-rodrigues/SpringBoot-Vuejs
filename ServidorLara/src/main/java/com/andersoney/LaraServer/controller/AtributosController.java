/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.controller;

import com.andersoney.LaraServer.model.Atributo;
import com.andersoney.LaraServer.services.AtributoService;
import com.andersoney.LaraServer.services.DefaultService;
import com.google.gson.Gson;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ander
 */
@RestController
@RequestMapping("/api/atributos")
@PreAuthorize("hasRole('USER')")
public class AtributosController implements ControllerDefault<Atributo, String> {

    private static final Logger log = LoggerFactory.getLogger(AtributosController.class);
    @Autowired
    AtributoService atributoService;

    @Override
    public ResponseEntity Atualizar(String id, Atributo object, BindingResult bindingResult) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResponseEntity cadastrar(Atributo object, BindingResult bindingResult) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResponseEntity excluir(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Atributo get(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Page<Atributo> getAllPaginator(Pageable pageable) {
        log.info("\n\nAcesso ao get pageable de Atributos.\n\n");
        Page<Atributo> obterAllPageable = atributoService.obterAllPageable(pageable);
        return obterAllPageable;
    }

    @Override
    public List<Atributo> obterTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
