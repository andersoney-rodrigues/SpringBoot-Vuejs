/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model;

import com.andersoney.LaraServer.model.ids.AtributoId;
import com.andersoney.LaraServer.services.UserService;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToMany;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author ander
 */
@Entity
@IdClass(value = AtributoId.class)
public class Atributo implements Serializable {

    @Id
    Long userid;

    @Id
    String codatributo;
    String name;

    @ElementCollection
    List<String> variacao;

    private boolean dif;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    List<Categoria> categorias;

    public Atributo() {
        categorias = new ArrayList<Categoria>();
        variacao = new ArrayList<String>();
        userid = UserService.getUserId();
    }

    public String getCodatributo() {
        return codatributo;
    }

    public void setCodatributo(String codatributo) {
        this.codatributo = codatributo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getVariacao() {
        return variacao;
    }

    public void setVariacao(List<String> variacao) {
        this.variacao = variacao;
    }

    public boolean isDif() {
        return dif;
    }

    public void setDif(boolean dif) {
        this.dif = dif;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

}
