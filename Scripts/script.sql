DROP DATABASE `laradb`;
CREATE SCHEMA `laradb`;
grant all privileges on laradb.* to laradb@'localhost' identified by 'admin123';
DROP DATABASE `laradbteste`;
CREATE SCHEMA `laradbteste`;
grant all privileges on laradbteste.* to laradb@'localhost' identified by 'admin123';