/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.controller;

import com.andersoney.LaraServer.services.DefaultService;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import com.andersoney.LaraServer.repository.UsuarioRepository;

/**
 *
 * @author ander
 */
public interface ControllerDefault<T, S> {

    @PutMapping(value = "/{id}")
    @ResponseBody
    ResponseEntity Atualizar(@PathVariable(value = "id") S id, @RequestBody
            @Valid T object, BindingResult bindingResult);

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity cadastrar(@RequestBody
            @Valid T object, BindingResult bindingResult);

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity excluir(@PathVariable(value = "id") S id);

    @GetMapping(value = "/{id}")
    @ResponseBody
    T get(S id);

    @GetMapping(value = "/list")
    @ResponseBody
    Page<T> getAllPaginator(Pageable pageable);

    @GetMapping
    @ResponseBody
    List<T> obterTodos();

}
