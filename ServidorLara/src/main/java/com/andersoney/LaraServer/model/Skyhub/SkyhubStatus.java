/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.Skyhub;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * SkyhubStatus do pedido
 *
 * @author ander
 */
@Entity
public class SkyhubStatus implements Serializable {

    @Id
    @GeneratedValue
    private Long ID;

    /**
     * Código de identificação do status
     */
    String code;
    /**
     * Descrição do status
     */
    String label;
    /**
     * Tipo do status (StatusType)
     */
    String type;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

}
