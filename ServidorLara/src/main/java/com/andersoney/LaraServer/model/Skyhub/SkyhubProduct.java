/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.model.Skyhub;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ander
 */
public class SkyhubProduct implements Serializable {

    private Long ID;

    String sku;
    String name;
    String description;
    String status;
    Float qty;
    Float price;
    Float promotional_price;
    Float cost;//Custo
    Float weight;
    Float height;
    Float width;
    Float length;
    String brand;//marca
    String ean;
    String nbm;
    ArrayList<SkyhubCategory> categories;//Categorias
    ArrayList<String> images;//na b2w usa uma string.
    ArrayList<SkyhubSpecifications> specifications;//Especificações
    ArrayList<SkyhubVariation> variations;//marketSt rucCategory
    ArrayList<String> variation_attributes;//marketStrucFamily

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getQty() {
        return qty;
    }

    public void setQty(Float qty) {
        this.qty = qty;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPromotional_price() {
        return promotional_price;
    }

    public void setPromotional_price(Float promotional_price) {
        this.promotional_price = promotional_price;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getNbm() {
        return nbm;
    }

    public void setNbm(String nbm) {
        this.nbm = nbm;
    }

    public ArrayList<SkyhubCategory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<SkyhubCategory> categories) {
        this.categories = categories;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public ArrayList<SkyhubSpecifications> getSpecifications() {
        return specifications;
    }

    public void setSpecifications(ArrayList<SkyhubSpecifications> specifications) {
        this.specifications = specifications;
    }

    public ArrayList<SkyhubVariation> getVariations() {
        return variations;
    }

    public void setVariations(ArrayList<SkyhubVariation> variations) {
        this.variations = variations;
    }

    public ArrayList<String> getVariation_attributes() {
        return variation_attributes;
    }

    public void setVariation_attributes(ArrayList<String> variation_attributes) {
        this.variation_attributes = variation_attributes;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

}
