/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andersoney.LaraServer.services;

import com.andersoney.LaraServer.model.Atributo;
import com.andersoney.LaraServer.model.Produto;
import com.andersoney.LaraServer.model.ProdutoAtributo;
import com.andersoney.LaraServer.model.Sku;
import com.andersoney.LaraServer.model.Variacao;
import com.andersoney.LaraServer.model.ids.AtributoId;
import com.andersoney.LaraServer.model.jobs.Operacoes;
import com.andersoney.LaraServer.model.jobs.ProdutosListas;
import com.andersoney.LaraServer.model.jobs.Tipo;
import com.andersoney.LaraServer.repository.ProdutoRepository;
import com.andersoney.LaraServer.repository.jobs.JobsProductRepository;
import com.google.gson.Gson;
import java.util.Date;
import java.util.List;
import javax.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author ander
 */
@Service
public class ProdutoService extends DefaultService<Produto, Long> {
    
    @Autowired
    ProdutoRepository produtoRepository;
    
    @Autowired
    AtributoService atributoService;
    
    @Autowired
    JobsProductRepository jobsProductRepository;
    
    @Autowired
    Validator validator;
    
    private static final Logger log = LoggerFactory.getLogger(CategoriaService.class);
    
    @Override
    public Produto cadastrar(Produto object) {
        object.setUserid(UserService.getUserId());
        for (ProdutoAtributo produtoAtributo : object.getProdutoAtributo()) {
            Atributo atributo = produtoAtributo.getAtributo();
            AtributoId key = new AtributoId(atributo.getCodatributo(), UserService.getUserId());
            Atributo obter = atributoService.obter(key);
            if (obter != null) {
                atributo = obter;
            } else {
                throw new UnsupportedOperationException("Objeto Atributo inexistente."); //To change body of generated methods, choose Tools | Templates.
            }
        }
        for (Sku sku : object.getSkus()) {
            for (Variacao variacoe : sku.getVariacoes()) {
                Atributo atributo = variacoe.getAtributo();
                AtributoId key = new AtributoId(atributo.getCodatributo(), UserService.getUserId());
                Atributo obter = atributoService.obter(key);
                if (obter != null) {
                    atributo = obter;
                } else {
                    throw new UnsupportedOperationException("Objeto Atributo inexistente."); //To change body of generated methods, choose Tools | Templates.
                }
            }
        }
        Produto save = produtoRepository.saveAndFlush(object);
        
        ProdutosListas produtosListas = new ProdutosListas();
        Gson gson = new Gson();
        Date dataAtual = new Date();
        
        produtosListas.setProduto(save);
        produtosListas.setCadastro(dataAtual);
//        produtosListas.setJson(gson.toJson(object));
        produtosListas.setOperacoes(Operacoes.CADASTRADO);
        produtosListas.setTipo(Tipo.PRODUTO);
        
        jobsProductRepository.save(produtosListas);
        
        return save;
    }
    
    @Override
    public Page<Produto> obterAllPageable(Long userid, Pageable pageable) {
        return produtoRepository.findAll(userid, pageable);
    }
    
    @Override
    public Produto obter(Long id) {
        return produtoRepository.findOne(id);
    }
    
    @Override
    public Produto atualizar(Long id, Produto produto) {
        Produto findOne = produtoRepository.findOne(id);
        Long userid = UserService.getUserId();
        if (findOne.getUserid() == userid) {
            produto.setID(id);
            produto.setUserid(userid);
            Produto save = produtoRepository.save(produto);
            ProdutosListas produtosListas = new ProdutosListas();
            Gson gson = new Gson();
            Date dataAtual = new Date();
            
            produtosListas.setProduto(save);
            produtosListas.setCadastro(dataAtual);
//            produtosListas.setJson(gson.toJson(save));
            produtosListas.setOperacoes(Operacoes.ATUALIZADO);
            produtosListas.setTipo(Tipo.PRODUTO);
            
            jobsProductRepository.save(produtosListas);
            
            return save;
        } else {
            throw new UnsupportedOperationException("Falha na segurança da requisição."); //To change body of generated methods, choose Tools | Templates. 
        }
        
    }
    
    @Override
    public Produto excluir(Long id) {
        Produto findOne = produtoRepository.findOne(id);
        this.produtoRepository.delete(findOne);
        return findOne;
    }
    
    @Override
    public List<Produto> obterTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
